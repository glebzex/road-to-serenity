using UnityEngine;
using Rewired;
using Random = UnityEngine.Random;


// This class is repsonsible for controlling inputs to the car.
[RequireComponent(typeof(DrivetrainCS))]
public class CarController : MonoBehaviour {

    // Add all wheels of the car here, so brake and steering forces can be applied to them.
    public WheelCS[] wheels;

    // A transform object which marks the car's center of gravity.
    // Cars with a higher CoG tend to tilt more in corners.
    // The further the CoG is towards the rear of the car, the more the car tends to oversteer. 
    // If this is not set, the center of mass is calculated from the colliders.
    public Transform centerOfMass;

    // A factor applied to the car's inertia tensor. 
    // Unity calculates the inertia tensor based on the car's collider shape.
    // This factor lets you scale the tensor, in order to make the car more or less dynamic.
    // A higher inertia makes the car change direction slower, which can make it easier to respond to.
    public float inertiaFactor = 1.5f;

    // current input state
    [HideInInspector]
    public float brake;
    [HideInInspector]
    private float throttle;

    private float throttleInput;
    private float clutch;
    [HideInInspector]
    public float steering;

    private float lastShiftTime = -1;
    [HideInInspector]
    public float handbrake;

    // cached DrivetrainCS reference
    private DrivetrainCS drivetrain;

    // How long the car takes to shift gears
    public float shiftSpeed = 0.8f;


    // These values determine how fast throttle value is changed when the accelerate keys are pressed or released.
    // Getting these right is important to make the car controllable, as keyboard input does not allow analogue input.
    // There are different values for when the wheels have full traction and when there are spinning, to implement 
    // traction control schemes.

    // How long it takes to fully engage the throttle
    public float throttleTime = 1.0f;
    // How long it takes to fully engage the throttle 
    // when the wheels are spinning (and traction control is disabled)
    public float throttleTimeTraction = 10.0f;
    // How long it takes to fully release the throttle
    public float throttleReleaseTime = 0.5f;
    // How long it takes to fully release the throttle 
    // when the wheels are spinning.
    public float throttleReleaseTimeTraction = 0.1f;

    // Turn traction control on or off
    public bool tractionControl = false;

    // Turn ABS control on or off
    public bool absControl = false;

    // These values determine how fast steering value is changed when the steering keys are pressed or released.
    // Getting these right is important to make the car controllable, as keyboard input does not allow analogue input.

    // How long it takes to fully turn the steering wheel from center to full lock
    public float steerTime = 1.2f;
    // This is added to steerTime per m/s of velocity, so steering is slower when the car is moving faster.
    public float veloSteerTime = 0.1f;

    // How long it takes to fully turn the steering wheel from full lock to center
    public float steerReleaseTime = 0.6f;
    // This is added to steerReleaseTime per m/s of velocity, so steering is slower when the car is moving faster.
    public float veloSteerReleaseTime = 0f;
    // When detecting a situation where the player tries to counter steer to correct an oversteer situation,
    // steering speed will be multiplied by the difference between optimal and current steering times this 
    // factor, to make the correction easier.
    public float steerCorrectionFactor = 4.0f;


    private bool gearShifted = false;
    private bool gearShiftedFlag = false;

    private Player _playerController;
    public static bool grounded;
    public float aircontrolSpeed = 20;
    public bool aircontrol = true;
    public float boostMultiplier = 5;
    public GameObject boostFX;
    private Rigidbody rb;
    public float boostSpeed = 250;
    public float flyingSpeed = 1250;
    public static bool isFlying;
    private float isFlyingTimer;
    public GameObject trails;
    private GameObject[] wheelMeshes = new GameObject[4];
    private Quaternion[] wheelBaseRots = new Quaternion[4];
    public float wheelRotAmount = 75, wheelRotTimer = -0.1f;
    public Transform[] targetFlyingRots;
    public Material carBodyMaterial, wingMaterial, bumperMaterial, chassisStuff;
    public bool randomizeColors = false;
    public Color mainColor, flyingColor, bumperMaterialGround, bumperMaterialFlying, chassisStuffGround, chassisStuffFlying;
    public float emissionIntensity = 7;
    public ParticleSystem[] boostColor = new ParticleSystem[8];
    public Light[] boostLight = new Light[4];
    private float startFlyTimer;
    public float flyingHeightLimit = 1000;
    private static CarController _instance;
	private Color prevMain, prevBumper, prevChassis, prevWing;
    private float colorChangeTimer;
    private bool randomColorLerp;

    public static Transform CarTransform
    {
        get
        {
            if (_instance==null || _instance.transform == null)
                return null;
            return _instance.transform;
        }
    }
    
    
    private void Awake()
    {
        if(_instance!=null)
            Destroy(gameObject);
        else
        {
            _instance = this;
        }
    }
    
    // Used by SoundController to get average slip velo of all wheels for skid sounds.
    public float slipVelo
    {
        get
        {
            float val = 0.0f;
            foreach (WheelCS w in wheels)
                val += w.slipVelo / wheels.Length;
            return val;
        }

    }

    // Initialize
    private void Start() {
        if (centerOfMass != null)
            GetComponent<Rigidbody>().centerOfMass = centerOfMass.localPosition;

        GetComponent<Rigidbody>().inertiaTensor *= inertiaFactor;
        drivetrain = GetComponent(typeof(DrivetrainCS)) as DrivetrainCS;
        
        _playerController = ReInput.players.GetPlayer(0);

        rb = GetComponent<Rigidbody>();

        for (int i = 0; i < wheels.Length; i++)
        {
            wheelMeshes[i] = wheels[i].transform.GetChild(0).gameObject;
            wheelBaseRots[i] = wheelMeshes[i].transform.rotation;
        }
        wingMaterial.SetFloat("_DissolveProg", Mathf.Lerp(0,1, wheelRotTimer));
    }

    private void Update() {

        // Steering
        Vector3 carDir = transform.forward;
        float fVelo = GetComponent<Rigidbody>().velocity.magnitude;
        Vector3 veloDir = GetComponent<Rigidbody>().velocity * (1 / fVelo);
        float angle = -Mathf.Asin(Mathf.Clamp(Vector3.Cross(veloDir, carDir).y, -1, 1));
        float optimalSteering = angle / (wheels[0].maxSteeringAngle * Mathf.Deg2Rad);
        if (fVelo < 1)
            optimalSteering = 0;

        float steerInput = 0;
        if (_playerController.GetAxis("Horizontal") < -0.2f)
            steerInput = -1;
        if (_playerController.GetAxis("Horizontal") > 0.2f)
            steerInput = 1;

        if (steerInput < steering) {
            float steerSpeed = (steering > 0) ? (1 / (steerReleaseTime + veloSteerReleaseTime * fVelo)) : (1 / (steerTime + veloSteerTime * fVelo));
            if (steering > optimalSteering)
                steerSpeed *= 1 + (steering - optimalSteering) * steerCorrectionFactor;
            steering -= steerSpeed * Time.deltaTime;
            if (steerInput > steering)
                steering = steerInput;
        } else if (steerInput > steering) {
            float steerSpeed = (steering < 0) ? (1 / (steerReleaseTime + veloSteerReleaseTime * fVelo)) : (1 / (steerTime + veloSteerTime * fVelo));
            if (steering < optimalSteering)
                steerSpeed *= 1 + (optimalSteering - steering) * steerCorrectionFactor;
            steering += steerSpeed * Time.deltaTime;
            if (steerInput < steering)
                steering = steerInput;
        }


        bool accelKey = _playerController.GetButton("Forward");
        bool brakeKey = _playerController.GetButton("Reverse");

        if (drivetrain.automatic && drivetrain.gear == 0) {
            accelKey = _playerController.GetButton("Reverse");
            brakeKey = _playerController.GetButton("Forward");
        }

        if (accelKey) {
            if (drivetrain.slipRatio < 0.10f)
                throttle += Time.deltaTime / throttleTime;
            else if (!tractionControl)
                throttle += Time.deltaTime / throttleTimeTraction;
            else
                throttle -= Time.deltaTime / throttleReleaseTime;

            if (throttleInput < 0)
                throttleInput = 0;
            throttleInput += Time.deltaTime / throttleTime;
        } else {
            if (drivetrain.slipRatio < 0.2f)
                throttle -= Time.deltaTime / throttleReleaseTime;
            else
                throttle -= Time.deltaTime / throttleReleaseTimeTraction;
        }

        throttle = Mathf.Clamp01(throttle);

        if (brakeKey) {
            if (drivetrain.slipRatio < 0.2f)
                brake += Time.deltaTime / throttleTime;
            else
                brake += Time.deltaTime / throttleTimeTraction;
            throttle = 0;
            throttleInput -= Time.deltaTime / throttleTime;
        } else {
            if (drivetrain.slipRatio < 0.2f)
                brake -= Time.deltaTime / throttleReleaseTime;
            else
                brake -= Time.deltaTime / throttleReleaseTimeTraction;
        }

        brake = Mathf.Clamp01(brake);
        throttleInput = Mathf.Clamp(throttleInput, -1, 1);

        // Handbrake
        handbrake = _playerController.GetButton("Handbrake") ? 1f : 0f;

        if (_playerController.GetButton("Handbrake") && !isFlying)
        {
            drivetrain.handbraking = true;
        }
        if ((_playerController.GetButton("HandbrakeL2") || Input.GetKeyDown(KeyCode.Space)) && isFlying)
        {
            rb.angularVelocity = Vector3.zero;
        }

        // Gear shifting
        float shiftThrottleFactor = Mathf.Clamp01((Time.time - lastShiftTime) / shiftSpeed);

        if (drivetrain.gear == 0 && _playerController.GetButton("Reverse")) {
            throttle = -1f;// Anti reverse lock thingy??
        }

        if (!isFlying)
        {
            if (_playerController.GetButton("TractionControl"))
            {
                throttle *= boostMultiplier;
                boostFX.SetActive(true);
                rb.AddForce(transform.forward * (boostSpeed * Time.deltaTime), ForceMode.Force);
            }
            else
            {
                boostFX.SetActive(false);
            }
        }
        else
        {
            if ((_playerController.GetButton("Forward") || Input.GetKey(KeyCode.LeftShift)) && transform.position.y < flyingHeightLimit)
            {
                rb.AddForce(transform.forward * (flyingSpeed * Time.deltaTime), ForceMode.Acceleration);
                    boostFX.SetActive(true);
            }
            else
            {
                boostFX.SetActive(false);
            }
        }

        if (drivetrain.gear == 0)
            drivetrain.throttle = _playerController.GetButton("Reverse") ? throttle : 0f;
        else
            drivetrain.throttle = _playerController.GetButton("Forward") ? (tractionControl ? throttle : 1) * shiftThrottleFactor : 0f;

        drivetrain.throttleInput = throttleInput;
        
/*
        if (Input.GetKeyDown(KeyCode.A)) {
            lastShiftTime = Time.time;
            drivetrain.ShiftUp();
        }

        if (Input.GetKeyDown(KeyCode.Z)) {
            lastShiftTime = Time.time;
            drivetrain.ShiftDown();
        }
*/
        
        //play gear shift sound
        if (gearShifted && gearShiftedFlag && drivetrain.gear != 1) {
            GetComponent<SoundController>().playShiftUp();
            gearShifted = false;
            gearShiftedFlag = false;
        }


        // ABS Trigger (This prototype version is used to prevent wheel lock , currently expiremental)
        if (absControl)
            brake -= brake >= 0.1f ? 0.1f : 0f;

        // Apply inputs
        foreach (WheelCS w in wheels) {
            w.brake = _playerController.GetButton("Reverse") ? brake :  0;
            w.handbrake = handbrake;
            w.steering = steering;
        }

        // Reset Car position and rotation in case it rolls over
        if (Input.GetKeyDown(KeyCode.R)) {
            transform.position = new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z);
            transform.rotation = Quaternion.Euler(0, transform.localRotation.y, 0);
        }
        
/*
        // Traction Control Toggle
        if (Input.GetKeyDown(KeyCode.T)) {

            if (tractionControl) {
                tractionControl = false;
            } else {
                tractionControl = true;
            }
        }

        // Anti-Brake Lock Toggle
        if (Input.GetKeyDown(KeyCode.B)) {
            if (absControl) {
                absControl = false;
            } else {
                absControl = true;
            }
        }*/
    }
    
    private void LateUpdate()
    {
        grounded = false;
        // Car aircontrol
        foreach (WheelCS w in wheels)
        {
            if (w.onGround)
            {
                grounded = true;
            }
        }

        if (wheelRotTimer > -0.1f)
        {
            for (int i = 0; i < wheelMeshes.Length; i++) 
            {
                wheelMeshes[i].transform.rotation = Quaternion.Lerp(wheels[i].transform.rotation, targetFlyingRots[i].rotation, wheelRotTimer);
            }

            carBodyMaterial.SetColor("_EmissionColor", Color.Lerp(mainColor * emissionIntensity, flyingColor * emissionIntensity, wheelRotTimer));
            bumperMaterial.SetColor("_EmissionColor", Color.Lerp(bumperMaterialGround * emissionIntensity, bumperMaterialFlying * emissionIntensity, wheelRotTimer));
            chassisStuff.SetColor("_EmissionColor", Color.Lerp(chassisStuffGround * emissionIntensity, chassisStuffFlying * emissionIntensity, wheelRotTimer));
            wingMaterial.SetFloat("_DissolveProg", Mathf.Lerp(0,1, wheelRotTimer));
        }
        if (isFlying)
        {

            if (wheelRotTimer >= 1)
            {
                wheelRotTimer = 1;
            }
            else
            {
                wheelRotTimer += Time.deltaTime;
            }
        }
        else
        {
            if (wheelRotTimer <= -0.1f)
            {
                wheelRotTimer = -0.1f;
            }
            else
            {
                wheelRotTimer -= Time.deltaTime;
            }
        }
        

        if (!grounded && aircontrol)
        {
            Aircontrol();
        }
        if (isFlyingTimer > 0.2f)
        {
            if (_playerController.GetButtonDown("AirToggle"))
            {
                isFlying = !isFlying;
                rb.angularVelocity = Vector3.zero;
            }
        }
        else
        {
            isFlying = false;
        }

        if (!grounded)
        {
            trails.SetActive(false);
            isFlyingTimer += Time.deltaTime;
        }
        else
        {
            if (startFlyTimer < 0)
            {
                isFlyingTimer = 0;
                trails.SetActive(true);
                
			    if(_playerController.GetButtonDown("AirToggle")){
				    rb.AddForce(transform.up * 15000, ForceMode.Impulse);
                    isFlying = true;
                    isFlyingTimer = 0.2001f;
                    startFlyTimer = 0.3f;
                }
            }
        }

        startFlyTimer -= Time.deltaTime;
        
        if (transform.position.y > flyingHeightLimit)
        {
            carBodyMaterial.SetColor("_EmissionColor", new Color(0,0,0,0));
            chassisStuff.SetColor("_EmissionColor", new Color(0,0,0,0));
            bumperMaterial.SetColor("_EmissionColor", new Color(0,0,0,0));
        }

        if(randomizeColors){
            randomizeColors = false;
            RandomizeCarColors();
        }

        if (randomColorLerp)
        {
            colorChangeTimer += Time.deltaTime;
            carBodyMaterial.SetColor("_EmissionColor", Color.Lerp(prevMain * emissionIntensity, mainColor * emissionIntensity, colorChangeTimer));
            bumperMaterial.SetColor("_EmissionColor", Color.Lerp(prevBumper * emissionIntensity, bumperMaterialGround * emissionIntensity, colorChangeTimer));
            chassisStuff.SetColor("_EmissionColor", Color.Lerp(prevChassis * emissionIntensity, chassisStuffGround * emissionIntensity, colorChangeTimer));
            if (colorChangeTimer >= 1)
            {
                randomColorLerp = false;
            }
        }
        else
        {
            colorChangeTimer = 0;
        }

    }

    private void Aircontrol()
    {
        float v = _playerController.GetAxis("Vertical");
        float h = -_playerController.GetAxis("Horizontal");
        float y = _playerController.GetAxis("YawAxis");

        if (isFlying)
        {
            transform.Rotate(new Vector3(v, y,h) * (aircontrolSpeed * Time.deltaTime));
        }
        else
        {
            transform.Rotate(new Vector3(v,0,h) * (aircontrolSpeed * Time.deltaTime));
        }
    }
    public void RandomizeCarColors()
    {
        randomColorLerp = true;
        prevMain = mainColor;
        prevChassis = chassisStuffGround;
        prevBumper = bumperMaterialGround;
            
        mainColor = Color.HSVToRGB(Random.Range(0,0.999f),1,1);
        flyingColor = Color.HSVToRGB(Random.Range(0,0.999f),1,1);
        bumperMaterialGround = Color.HSVToRGB(Random.Range(0,0.999f),1,1);
        bumperMaterialFlying = Color.HSVToRGB(Random.Range(0,0.999f),1,1);
        chassisStuffGround = Color.HSVToRGB(Random.Range(0,0.999f),1,1);
        chassisStuffFlying = Color.HSVToRGB(Random.Range(0,0.999f),1,1);
        wingMaterial.SetColor("_EmissionColor", Color.HSVToRGB(Random.Range(0,0.999f),1,1));

        Gradient gradient = new Gradient();

        for (int i = 0; i < boostColor.Length; i++)
        {   
            var col = boostColor[i].colorOverLifetime;
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(new Color(1,1,1), 0.0f), new GradientColorKey(chassisStuffGround, 0.262f), 
                new GradientColorKey(bumperMaterialGround, 0.497f), new GradientColorKey(mainColor, 1.0f)}, 
                new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(0.59f, 0.129f),
                new GradientAlphaKey(0.59f, 0.832f), new GradientAlphaKey(0.0f, 1.0f) });
            col.color = gradient;
        }
        for (int i = 0; i < boostLight.Length; i++)
        {
            boostLight[i].color = mainColor;
        }
    }
}
