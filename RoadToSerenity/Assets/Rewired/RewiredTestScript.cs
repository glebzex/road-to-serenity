﻿using UnityEngine;
using Rewired;

public class RewiredTestScript : MonoBehaviour
{
    public int playerId = 0;
    
    // Test Axis
    public float ad, qe;
    // Test Buttons
    public bool w, s, z;

    private Player _player;
    // Start is called before the first frame update
    private void Awake()
    {
        _player = ReInput.players.GetPlayer(playerId);
    }

    // Update is called once per frame
    private void Update()
    {
        ad = _player.GetAxis("Horizontal");
        qe = _player.GetAxis("Vertical");
        if (_player.GetButton("Forward"))
        {
            w = true;
        }
        else
        {
            w = false;
        }
        if (_player.GetButton("Reverse"))
        {
            s = true;
        }
        else
        {
            s = false;
        }
        if (_player.GetButton("Menu"))
        {
            z = true;
        }
        else
        {
            z = false;
        }
    }
}
