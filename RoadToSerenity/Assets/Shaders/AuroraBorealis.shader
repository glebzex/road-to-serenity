// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:1,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:7759,x:33199,y:32714,varname:node_7759,prsc:2|emission-5217-OUT,voffset-3975-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:1788,x:30966,y:32733,ptovrint:False,ptlb:MIX,ptin:_MIX,varname:node_1788,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:f06709e1b293a5b429206e4612af600f,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:6335,x:31511,y:33447,varname:node_6335,prsc:2,tex:f06709e1b293a5b429206e4612af600f,ntxv:0,isnm:False|UVIN-9362-OUT,TEX-1788-TEX;n:type:ShaderForge.SFN_ValueProperty,id:4640,x:32362,y:33220,ptovrint:False,ptlb:VO_Strength,ptin:_VO_Strength,varname:node_4640,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.05;n:type:ShaderForge.SFN_Multiply,id:4808,x:32362,y:33074,varname:node_4808,prsc:2|A-6335-R,B-4640-OUT;n:type:ShaderForge.SFN_Append,id:3975,x:32539,y:33074,varname:node_3975,prsc:2|A-4160-OUT,B-4808-OUT;n:type:ShaderForge.SFN_Vector1,id:4160,x:32539,y:33198,varname:node_4160,prsc:2,v1:0;n:type:ShaderForge.SFN_TexCoord,id:5684,x:30966,y:32335,varname:node_5684,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ValueProperty,id:8615,x:30828,y:33232,ptovrint:False,ptlb:VO_SPEED_U,ptin:_VO_SPEED_U,varname:node_8615,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:6939,x:30828,y:33312,ptovrint:False,ptlb:vO_SPEED_V,ptin:_vO_SPEED_V,varname:node_6939,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:5658,x:31009,y:33234,varname:node_5658,prsc:2|A-8615-OUT,B-6939-OUT;n:type:ShaderForge.SFN_Multiply,id:5331,x:31177,y:33234,varname:node_5331,prsc:2|A-5658-OUT,B-2117-T;n:type:ShaderForge.SFN_Time,id:2117,x:30966,y:32549,varname:node_2117,prsc:2;n:type:ShaderForge.SFN_Add,id:9031,x:31341,y:33234,varname:node_9031,prsc:2|A-5331-OUT,B-5684-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:4284,x:31505,y:32853,varname:node_4284,prsc:2,tex:f06709e1b293a5b429206e4612af600f,ntxv:0,isnm:False|TEX-1788-TEX;n:type:ShaderForge.SFN_Tex2d,id:9589,x:31505,y:32716,varname:node_9589,prsc:2,tex:f06709e1b293a5b429206e4612af600f,ntxv:0,isnm:False|UVIN-4226-OUT,TEX-1788-TEX;n:type:ShaderForge.SFN_ValueProperty,id:2229,x:30982,y:32176,ptovrint:False,ptlb:Indentations_Speed_U,ptin:_Indentations_Speed_U,varname:node_2229,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:24,x:30982,y:32260,ptovrint:False,ptlb:Indentantions_Speed_V,ptin:_Indentantions_Speed_V,varname:node_24,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:1736,x:31181,y:32176,varname:node_1736,prsc:2|A-2229-OUT,B-24-OUT;n:type:ShaderForge.SFN_Multiply,id:4809,x:31362,y:32176,varname:node_4809,prsc:2|A-1736-OUT,B-2117-T;n:type:ShaderForge.SFN_Add,id:8276,x:31539,y:32176,varname:node_8276,prsc:2|A-4809-OUT,B-5684-UVOUT;n:type:ShaderForge.SFN_ValueProperty,id:3244,x:32031,y:32600,ptovrint:False,ptlb:Indentation_Add,ptin:_Indentation_Add,varname:node_3244,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Add,id:2041,x:32031,y:32649,varname:node_2041,prsc:2|A-3244-OUT,B-4200-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2375,x:30975,y:31514,ptovrint:False,ptlb:Lines_Speed_U,ptin:_Lines_Speed_U,varname:_Indentations_Speed_U_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.1;n:type:ShaderForge.SFN_ValueProperty,id:5063,x:30975,y:31598,ptovrint:False,ptlb:Lines_Speed_V,ptin:_Lines_Speed_V,varname:_Indentantions_Speed_V_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:2502,x:31174,y:31514,varname:node_2502,prsc:2|A-2375-OUT,B-5063-OUT;n:type:ShaderForge.SFN_Multiply,id:8510,x:31355,y:31514,varname:node_8510,prsc:2|A-2502-OUT,B-2117-T;n:type:ShaderForge.SFN_Add,id:8678,x:31532,y:31514,varname:node_8678,prsc:2|A-8510-OUT,B-5684-UVOUT;n:type:ShaderForge.SFN_Tex2d,id:1077,x:31505,y:32587,varname:node_1077,prsc:2,tex:f06709e1b293a5b429206e4612af600f,ntxv:0,isnm:False|UVIN-165-OUT,TEX-1788-TEX;n:type:ShaderForge.SFN_Add,id:1081,x:32290,y:32550,varname:node_1081,prsc:2|A-8423-OUT,B-2041-OUT;n:type:ShaderForge.SFN_Multiply,id:9315,x:32486,y:32775,varname:node_9315,prsc:2|A-1081-OUT,B-4284-G;n:type:ShaderForge.SFN_Multiply,id:8423,x:32073,y:32409,varname:node_8423,prsc:2|A-9808-OUT,B-1077-A;n:type:ShaderForge.SFN_ValueProperty,id:9808,x:32073,y:32363,ptovrint:False,ptlb:Lines_Strength,ptin:_Lines_Strength,varname:node_9808,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_ValueProperty,id:4556,x:31156,y:31388,ptovrint:False,ptlb:Lines_Tile_U,ptin:_Lines_Tile_U,varname:node_4556,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:4;n:type:ShaderForge.SFN_ValueProperty,id:7467,x:31156,y:31464,ptovrint:False,ptlb:Lines_Tile_V,ptin:_Lines_Tile_V,varname:node_7467,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:8187,x:31355,y:31388,varname:node_8187,prsc:2|A-4556-OUT,B-7467-OUT;n:type:ShaderForge.SFN_Multiply,id:165,x:31532,y:31388,varname:node_165,prsc:2|A-8187-OUT,B-8678-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5423,x:31192,y:32134,ptovrint:False,ptlb:Indentantios_Tile_V,ptin:_Indentantios_Tile_V,varname:node_5423,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:7679,x:31192,y:32058,ptovrint:False,ptlb:Indentantios_Tile_U,ptin:_Indentantios_Tile_U,varname:node_7679,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Append,id:2925,x:31367,y:32058,varname:node_2925,prsc:2|A-7679-OUT,B-5423-OUT;n:type:ShaderForge.SFN_Multiply,id:4226,x:31539,y:32058,varname:node_4226,prsc:2|A-2925-OUT,B-8276-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8664,x:30950,y:33447,ptovrint:False,ptlb:VO_Tile_U,ptin:_VO_Tile_U,varname:node_8664,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:3003,x:30950,y:33525,ptovrint:False,ptlb:VO_Tile_V,ptin:_VO_Tile_V,varname:node_3003,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:5481,x:31130,y:33447,varname:node_5481,prsc:2|A-8664-OUT,B-3003-OUT;n:type:ShaderForge.SFN_Multiply,id:9362,x:31341,y:33447,varname:node_9362,prsc:2|A-9031-OUT,B-5481-OUT;n:type:ShaderForge.SFN_Lerp,id:7968,x:32728,y:32400,varname:node_7968,prsc:2|A-5453-RGB,B-7290-RGB,T-6735-OUT;n:type:ShaderForge.SFN_Slider,id:4697,x:32294,y:32354,ptovrint:False,ptlb:ColorSlider,ptin:_ColorSlider,varname:node_4697,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:2;n:type:ShaderForge.SFN_Multiply,id:6735,x:32531,y:32550,varname:node_6735,prsc:2|A-4697-OUT,B-9315-OUT;n:type:ShaderForge.SFN_Color,id:5453,x:32564,y:31880,ptovrint:False,ptlb:Color_1,ptin:_Color_1,varname:node_5453,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:7290,x:32564,y:32044,ptovrint:False,ptlb:Color_2,ptin:_Color_2,varname:node_7290,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0.9201374,c4:1;n:type:ShaderForge.SFN_Multiply,id:4089,x:32781,y:32794,varname:node_4089,prsc:2|A-7968-OUT,B-9315-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3226,x:32983,y:32934,ptovrint:False,ptlb:Emission_Strength,ptin:_Emission_Strength,varname:node_3226,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_Multiply,id:5217,x:32983,y:32807,varname:node_5217,prsc:2|A-4089-OUT,B-3226-OUT;n:type:ShaderForge.SFN_Multiply,id:4200,x:31735,y:32716,varname:node_4200,prsc:2|A-9589-B,B-1310-OUT;n:type:ShaderForge.SFN_Vector1,id:1310,x:31735,y:32835,varname:node_1310,prsc:2,v1:25;proporder:1788-4640-8615-6939-2229-24-3244-2375-5063-9808-4556-7467-5423-7679-8664-3003-4697-5453-7290-3226;pass:END;sub:END;*/

Shader "Custom/AuroraBorealis" {
    Properties {
        _MIX ("MIX", 2D) = "white" {}
        _VO_Strength ("VO_Strength", Float ) = 0.05
        _VO_SPEED_U ("VO_SPEED_U", Float ) = 0
        _vO_SPEED_V ("vO_SPEED_V", Float ) = 0
        _Indentations_Speed_U ("Indentations_Speed_U", Float ) = 0.1
        _Indentantions_Speed_V ("Indentantions_Speed_V", Float ) = 0
        _Indentation_Add ("Indentation_Add", Float ) = 0.5
        _Lines_Speed_U ("Lines_Speed_U", Float ) = 0.1
        _Lines_Speed_V ("Lines_Speed_V", Float ) = 0
        _Lines_Strength ("Lines_Strength", Float ) = 2
        _Lines_Tile_U ("Lines_Tile_U", Float ) = 4
        _Lines_Tile_V ("Lines_Tile_V", Float ) = 1
        _Indentantios_Tile_V ("Indentantios_Tile_V", Float ) = 1
        _Indentantios_Tile_U ("Indentantios_Tile_U", Float ) = 3
        _VO_Tile_U ("VO_Tile_U", Float ) = 0
        _VO_Tile_V ("VO_Tile_V", Float ) = 0
        _ColorSlider ("ColorSlider", Range(0, 2)) = 1
        _Color_1 ("Color_1", Color) = (0,1,1,1)
        _Color_2 ("Color_2", Color) = (1,0,0.9201374,1)
        _Emission_Strength ("Emission_Strength", Float ) = 1.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "DisableBatching"="True"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MIX; uniform float4 _MIX_ST;
            uniform float _VO_Strength;
            uniform float _VO_SPEED_U;
            uniform float _vO_SPEED_V;
            uniform float _Indentations_Speed_U;
            uniform float _Indentantions_Speed_V;
            uniform float _Indentation_Add;
            uniform float _Lines_Speed_U;
            uniform float _Lines_Speed_V;
            uniform float _Lines_Strength;
            uniform float _Lines_Tile_U;
            uniform float _Lines_Tile_V;
            uniform float _Indentantios_Tile_V;
            uniform float _Indentantios_Tile_U;
            uniform float _VO_Tile_U;
            uniform float _VO_Tile_V;
            uniform float _ColorSlider;
            uniform float4 _Color_1;
            uniform float4 _Color_2;
            uniform float _Emission_Strength;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_2117 = _Time;
                float2 node_9362 = (((float2(_VO_SPEED_U,_vO_SPEED_V) * node_2117.g)+o.uv0) * float2(_VO_Tile_U,_VO_Tile_V));
                float4 node_6335 = tex2Dlod(_MIX,float4(TRANSFORM_TEX(node_9362, _MIX),0.0,0));
                v.vertex.xyz += float3(float2(0.0,(node_6335.r * _VO_Strength)),0.0);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_2117 = _Time;
                float2 node_165 = (float2(_Lines_Tile_U,_Lines_Tile_V) * ((float2(_Lines_Speed_U,_Lines_Speed_V) * node_2117.g)+i.uv0));
                float4 node_1077 = tex2D(_MIX,TRANSFORM_TEX(node_165, _MIX));
                float2 node_4226 = (float2(_Indentantios_Tile_U,_Indentantios_Tile_V) * ((float2(_Indentations_Speed_U,_Indentantions_Speed_V) * node_2117.g)+i.uv0));
                float4 node_9589 = tex2D(_MIX,TRANSFORM_TEX(node_4226, _MIX));
                float4 node_4284 = tex2D(_MIX,TRANSFORM_TEX(i.uv0, _MIX));
                float node_9315 = (((_Lines_Strength * node_1077.a)+(_Indentation_Add+(node_9589.b * 25.0))) * node_4284.g);
                float3 emissive = ((lerp(_Color_1.rgb, _Color_2.rgb, (_ColorSlider * node_9315)) * node_9315) * _Emission_Strength);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _MIX; uniform float4 _MIX_ST;
            uniform float _VO_Strength;
            uniform float _VO_SPEED_U;
            uniform float _vO_SPEED_V;
            uniform float _VO_Tile_U;
            uniform float _VO_Tile_V;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_2117 = _Time;
                float2 node_9362 = (((float2(_VO_SPEED_U,_vO_SPEED_V) * node_2117.g)+o.uv0) * float2(_VO_Tile_U,_VO_Tile_V));
                float4 node_6335 = tex2Dlod(_MIX,float4(TRANSFORM_TEX(node_9362, _MIX),0.0,0));
                v.vertex.xyz += float3(float2(0.0,(node_6335.r * _VO_Strength)),0.0);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
