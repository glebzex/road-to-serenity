// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,fgcg:0,fgcb:0,fgca:1,fgde:0,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:8215,x:32321,y:32359,varname:node_8215,prsc:2|emission-773-OUT,alpha-4634-A;n:type:ShaderForge.SFN_Fresnel,id:5264,x:30944,y:32673,varname:node_5264,prsc:2|EXP-8195-OUT;n:type:ShaderForge.SFN_Multiply,id:2053,x:31339,y:32639,varname:node_2053,prsc:2|A-4634-RGB,B-5671-OUT;n:type:ShaderForge.SFN_Color,id:4634,x:30652,y:32509,ptovrint:False,ptlb:FresnelColor,ptin:_FresnelColor,varname:node_4634,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9433962,c2:0.06674972,c3:0.06674972,c4:1;n:type:ShaderForge.SFN_OneMinus,id:5671,x:31137,y:32673,varname:node_5671,prsc:2|IN-5264-OUT;n:type:ShaderForge.SFN_Fresnel,id:7962,x:30944,y:32813,varname:node_7962,prsc:2|EXP-286-OUT;n:type:ShaderForge.SFN_Multiply,id:8739,x:31137,y:32813,varname:node_8739,prsc:2|A-7962-OUT,B-4634-RGB;n:type:ShaderForge.SFN_Multiply,id:773,x:31537,y:32687,varname:node_773,prsc:2|A-2053-OUT,B-8739-OUT,C-5701-OUT;n:type:ShaderForge.SFN_Vector1,id:5701,x:31375,y:32813,varname:node_5701,prsc:2,v1:8;n:type:ShaderForge.SFN_Time,id:2670,x:29147,y:32741,varname:node_2670,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:4508,x:29339,y:32919,ptovrint:False,ptlb:FadeSpeed,ptin:_FadeSpeed,varname:node_4508,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_OneMinus,id:2449,x:29929,y:32761,varname:node_2449,prsc:2|IN-744-OUT;n:type:ShaderForge.SFN_Multiply,id:3734,x:29339,y:32761,varname:node_3734,prsc:2|A-2670-T,B-4508-OUT;n:type:ShaderForge.SFN_RemapRange,id:744,x:29722,y:32761,varname:node_744,prsc:2,frmn:0,frmx:1,tomn:1.5,tomx:2|IN-5526-OUT;n:type:ShaderForge.SFN_RemapRange,id:8195,x:30233,y:32674,varname:node_8195,prsc:2,frmn:0,frmx:1,tomn:2.5,tomx:3|IN-2449-OUT;n:type:ShaderForge.SFN_Sin,id:5526,x:29529,y:32761,varname:node_5526,prsc:2|IN-3734-OUT;n:type:ShaderForge.SFN_RemapRange,id:286,x:30233,y:32854,varname:node_286,prsc:2,frmn:0,frmx:1,tomn:1,tomx:2|IN-2449-OUT;proporder:4634-4508;pass:END;sub:END;*/

Shader "Custom/MovingPortal" {
    Properties {
        _FresnelColor ("FresnelColor", Color) = (0.9433962,0.06674972,0.06674972,1)
        _FadeSpeed ("FadeSpeed", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _FresnelColor;
            uniform float _FadeSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_2670 = _Time;
                float node_3734 = (node_2670.g * _FadeSpeed);
                float node_2449 = (1.0 - (sin(node_3734)*0.5+1.5));
                float node_8195 = (node_2449*0.5+2.5);
                float3 emissive = ((_FresnelColor.rgb * (1.0 - pow(1.0-max(0,dot(normalDirection, viewDirection)),node_8195))) * (pow(1.0-max(0,dot(normalDirection, viewDirection)),(node_2449*1.0+1.0)) * _FresnelColor.rgb) * 8.0);
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,_FresnelColor.a);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
