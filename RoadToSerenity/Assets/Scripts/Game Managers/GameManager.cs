﻿using Spawn;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public KeyCode restartKey = KeyCode.R;
    public KeyCode respawnKey = KeyCode.Space;

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyUp(restartKey))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            Debug.Log("Restarting");
        }

        if (Input.GetKeyUp(respawnKey))
        {
            FindObjectOfType<RaycastSpawn>().Spawn();
        }
    }
}