﻿using UnityEngine;

public class SpawnCar : MonoBehaviour
{
    public GameObject carToSpawn;

    // Start is called before the first frame update
    private void Start()
    {
        if (Singleton.Instance != null)
        {
//            Debug.Log("No car spawned");
        }
        else
        {
//            Debug.Log("Spawning car");
            Instantiate(carToSpawn, transform.position, transform.rotation);
        }
    }
}