﻿using System.Collections;
using Portal;
using UnityEngine;
using WorldGeneration;

namespace Spawn
{
    public class RaycastSpawn : MonoBehaviour
    {
        private void Update()
        {
            if (transform.position.y < -3f && transform.position.y > -100f)
            {
                TrySpawn();
            }
        }

        public void Spawn()
        {
            StartCoroutine(nameof(FindTopPointAndSpawn));
        }

        private IEnumerator FindTopPointAndSpawn()
        {
            yield return new WaitForEndOfFrame();
            TrySpawn();
        }

        private void TrySpawn()
        {
            var transformCache = transform;
            if (Physics.Raycast(new Vector3(transformCache.position.x, 1000f, transformCache.position.z), Vector3.down,
                                out var hit, 1500f,
                                LayerMask.GetMask("Ground")))
            {
                var newPosition = new Vector3(hit.point.x, hit.point.y + 2f, hit.point.z);
                transformCache.position = newPosition;
                transformCache.rotation = Quaternion.identity;
                var rigidBody = GetComponent<Rigidbody>();
                rigidBody.velocity        = Vector3.zero;
                rigidBody.angularVelocity = Vector3.zero;
            }
            else
            {
                Debug.LogWarning(
                    "No ground found under the car! (Didn't preload or terrain level too high?) Trying to spawn at Spawn Point");
                var spawnPoint = PortalManager.instance.PortalList.Find(t =>
                                                                            t.GetComponent<PortalTeleporter>()
                                                                             .portalType == PortalType.SpawnPoint);
                if (spawnPoint)
                {
                    transform.position = spawnPoint.position;
                }
                else
                {
                    Debug.LogWarning(
                        "No ground found under the car! (Didn't preload or terrain level too high?) Trying to build the terrain at players position");
                    WorldGenerator.PreloadMapAtPoint(transformCache.position);
                    TrySpawn();
                }
            }
        }
    }
}