﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class VisualEq : MonoBehaviour
{
    private static readonly float[] normalizeMaxValues    = new float[8];
    private static readonly float[] bufferedDecreaseValue = new float[8];
    private static readonly float[] freqBands             = new float[8];
    public                  float   smoothFactor          = 1.5f;

    private readonly float[]     _clipSampleData = new float[512];
    private          AudioSource _source;

    public static float[] BufferedFreqBands { get; } = new float[8];

    public static float[] NormalizedBands { get; } = new float[8];

    // Start is called before the first frame update
    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        GetAudioSamples();
        CreateFrequencyBands();
        CreateBufferedBands();
        CreateNormalizedBands();

//        DrawEightBands(normalizedFreqBands, 10f, 0f, Color.green);
    }

    private void GetAudioSamples()
    {
        _source.GetSpectrumData(_clipSampleData, 0, FFTWindow.Blackman);
    }

    private void CreateNormalizedBands()
    {
        for (var i = 0; i < 8; i++)
        {
            if (BufferedFreqBands[i] > normalizeMaxValues[i])
            {
                normalizeMaxValues[i] = BufferedFreqBands[i];
            }

            NormalizedBands[i] = BufferedFreqBands[i] / normalizeMaxValues[i];
        }
    }

    private void CreateBufferedBands()
    {
        for (var i = 0; i < 8; i++)
        {
            if (BufferedFreqBands[i] < freqBands[i])
            {
                BufferedFreqBands[i]     = freqBands[i];
                bufferedDecreaseValue[i] = 0.005f;
            }

            if (BufferedFreqBands[i] > freqBands[i])
            {
                if (BufferedFreqBands[i] - bufferedDecreaseValue[i] >= 0f)
                {
                    BufferedFreqBands[i]     -= bufferedDecreaseValue[i];
                    bufferedDecreaseValue[i] *= smoothFactor;
                }
                else
                {
                    BufferedFreqBands[i] = 0f;
                }
            }
        }
    }

    private void DrawEightBands(float[] array, float yMultiplier, float yOffset, Color color)
    {
        for (var i = 0; i < array.Length; i++)
        {
            Debug.DrawLine(new Vector3(20f * i, yMultiplier * array[i] + yOffset, 0f),
                           new Vector3(20f                  * i + 20f, yMultiplier * array[i] + yOffset, 0f), color);
        }
    }

    private void CreateFrequencyBands()
    {
        int count = 0, sampleCount = 2;
        for (var i = 0; i < 8; i++)
        {
            var average = 0f;

            if (i == 7)
            {
                sampleCount += 2;
            }

            for (var j = 0; j < sampleCount; j++)
            {
                average += _clipSampleData[count] * (count + 1);
                count++;
            }

            average      /= count;
            freqBands[i] =  average;

            sampleCount *= 2;
        }
    }
}