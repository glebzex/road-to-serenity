﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ChangePostFx : MonoBehaviour
{
    public PostProcessProfile postProcessProfile;

    public float minBloom = 5f, maxBloom = 15f;

    [Range(0, 7)]
    public int band = 1;

    private Bloom _bloom;

    // Start is called before the first frame update
    private void Start()
    {
        _bloom = (Bloom)postProcessProfile.settings.Find(t => t is Bloom);
    }

    // Update is called once per frame
    private void Update()
    {
//        var deviator = new[] {VisualEq.NormalizedBands[0], VisualEq.NormalizedBands[1], VisualEq.NormalizedBands[2]}.Average();

        _bloom.intensity.value =
            minBloom + (maxBloom - minBloom) * VisualEq.BufferedFreqBands[band];
    }
}