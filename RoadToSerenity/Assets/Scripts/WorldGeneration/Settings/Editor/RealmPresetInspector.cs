﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RealmPreset))]
public class RealmPresetInspector : Editor
{
    private Object   _meshGeneratorObject,       _roadsPresetObject;
    private Editor   _meshGeneratorPresetEditor, _roadsPresetEditor;
    private RoadType _roadType;
    private bool     _showMesh, _showRoads;


    private void OnEnable()
    {
        LoadEditors();
    }

    private void LoadEditors()
    {
        _meshGeneratorObject = serializedObject.FindProperty("meshGeneratorPreset").objectReferenceValue;
        _roadsPresetObject   = serializedObject.FindProperty("roadsPreset").objectReferenceValue;

        _meshGeneratorPresetEditor = CreateEditor(_meshGeneratorObject);
        _roadsPresetEditor         = CreateEditor(_roadsPresetObject);
    }

    public override void OnInspectorGUI()
    {
        if (serializedObject.FindProperty("meshGeneratorPreset").objectReferenceValue != _meshGeneratorObject ||
            serializedObject.FindProperty("roadsPreset").objectReferenceValue         != _roadsPresetObject)
        {
            LoadEditors();
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("skybox"),              true);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("meshGeneratorPreset"), true);

        _showMesh = EditorGUILayout.Foldout(_showMesh, "Mesh Generator Preset");
        if (_showMesh)
        {
            EditorGUI.indentLevel++;
            _meshGeneratorPresetEditor.OnInspectorGUI();
            EditorGUI.indentLevel--;
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("roadsPreset"), true);

        _showRoads = EditorGUILayout.Foldout(_showRoads, "Roads Preset");
        if (_showRoads)
        {
            EditorGUI.indentLevel++;
            _roadsPresetEditor.OnInspectorGUI();
            EditorGUI.indentLevel--;
        }


        serializedObject.ApplyModifiedProperties();
    }
}