using UnityEditor;

[CustomEditor(typeof(RoadsPreset))]
public class RoadsPresetInspector : Editor
{
    private RoadType _roadType;

    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("roadType"), true);

        switch (serializedObject.FindProperty("roadType").enumValueIndex)
        {
            case 0:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("roadWebPreset"), true);
                break;
            case 1:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("roadGridPreset"), true);
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}