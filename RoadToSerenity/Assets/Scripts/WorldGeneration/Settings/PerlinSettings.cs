using System;
using UnityEngine;

[Serializable]
public class PerlinSettings
{
    public bool  usePerlin        = true;
    public float perlinMultiplier = 100f;
    public float perlinDivisionX  = 80f, perlinDivisionY = 80f;

    [Range(0f, 1f)]
    public float heightThreshold = 0.4f;

    [Range(0, 50)]
    public int heightDiscretion = 2;
}