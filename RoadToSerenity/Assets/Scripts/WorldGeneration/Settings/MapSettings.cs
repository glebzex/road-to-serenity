using System;
using UnityEngine;

[Serializable]
public class MapSettings
{
    public int preloadRadius = 4;
    public int gridSize      = 30;

    [Range(0, 127)]
    public int mapWidth = 127, mapHeight = 127;

    [Range(1, 100)]
    public int blocksX = 5, blocksY = 5;

    [Tooltip("The more the number the larger the grid looks")]
    public int materialResolution;
}