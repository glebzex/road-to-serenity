﻿using System;
using UnityEngine;
using Utils;

public enum RoadType
{
    Web,
    Grid
}

[Serializable]
public class RoadWebPreset
{
    public float yOffset = 0.5f;

//    public float cityChance = 10;
//
//    public GameObject mainCityBlock;
//    public GameObject endRowCityBlock;
//    public GameObject lastRowCityBlock;
//    public GameObject lastCityBlock;

    public RandomPrefab[] roadPrefabs;
}

[Serializable]
public class RoadGridPreset
{
    public float yOffset = 0.5f;

    public int          gridSize = 10;
    public GameObject[] fourWayRoad;
    public GameObject   straightRoad;
}

[CreateAssetMenu(fileName = "Roads Preset", menuName = "World Generator/Roads Preset")]
public class RoadsPreset : ScriptableObject
{
    [SerializeField]
    private RoadType roadType;

    [SerializeField]
    private RoadWebPreset roadWebPreset;

    [SerializeField]
    private RoadGridPreset roadGridPreset;

    public RoadType RoadType => roadType;

    public RoadWebPreset RoadWebPreset => roadWebPreset;

    public RoadGridPreset RoadGridPreset => roadGridPreset;
}