﻿using UnityEngine;

[CreateAssetMenu(fileName = "Realm Preset", menuName = "World Generator/Realm Preset")]
public class RealmPreset : ScriptableObject
{
    [SerializeField]
    private Material[] skybox;

    [SerializeField]
    private MeshGeneratorPreset meshGeneratorPreset;

    [SerializeField]
    private RoadsPreset roadsPreset;

    public RoadsPreset RoadsPreset => roadsPreset;

    public MeshGeneratorPreset MeshGeneratorPreset => meshGeneratorPreset;

    public Material[] Skybox => skybox;
}