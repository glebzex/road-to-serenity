﻿using UnityEngine;
using Utils;

[CreateAssetMenu(fileName = "Mesh Preset", menuName = "World Generator/Mesh Preset")]
public class MeshGeneratorPreset : ScriptableObject
{
    public GameObject groundPrefab;

    public MapSettings    mapSettings;
    public PerlinSettings perlinSettings;

    public int spawnObjectsNumber = 20;

    [SerializeField]
    private RandomPrefab[] spawnObjects;


    private WeightedRandom<GameObject> randomItems;

    public WeightedRandom<GameObject> SpawnObjects
    {
        get
        {
            if (randomItems == null)
            {
                randomItems = new WeightedRandom<GameObject>(spawnObjects);
            }

            return randomItems;
        }
    }
}