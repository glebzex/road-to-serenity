﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Portal;
using UnityEngine;
using Random = UnityEngine.Random;

//using Unity.Mathematics;


namespace WorldGeneration
{
    [Serializable]
    public class SpawnObject
    {
        public GameObject objectPrefab;

        [Range(0, 1000)]
        public int spawnChance;
    }

    public class WorldGenerator : MonoBehaviour
    {
        public delegate void CoordinateOperation(int x, int y);

        public static readonly Dictionary<Tuple<int, int>, GameObject> MapPieces = new();

        private static float _seed;

        public static  MeshGeneratorPreset meshGeneratorPreset;
        private static GameObject          _template;

        public static WorldGenerator _instance;

        private static readonly List<Tuple<int, int>> _currentPreloadCircle = new();
        public                  MeshGeneratorPreset   mapPreset;

        private GameObject _objectParent;

        private IEnumerator destroyBuffer;

        private IEnumerator preloadBuffer;

        private void Start()
        {
            if (_instance == null)
            {
                _instance = this;
            }

            InitialSetup();

            BuildBasicMap();

            StartCoroutine(nameof(PreloadMap));
        }

        private IEnumerator PreloadMap()
        {
            yield return new WaitForEndOfFrame();
            PreloadMapAtPoint(PortalManager.instance.PortalList
                                           .FirstOrDefault(t => t.GetComponent<PortalTeleporter>().portalType ==
                                                               PortalType.SpawnTunnelOut)
                                           .transform
                                           .position);
        }

        private void InitialSetup()
        {
            //Set random state
//            var rngState = Random.Range(Int32.MinValue, Int32.MaxValue);
//            Random.InitState(rngState);
            CalculateSeed();
//            seed = 1;

            //Make a parent for all spawned items
            _objectParent = new GameObject { name = "Spawn Object Parent" };

            //Set the preset
            meshGeneratorPreset = mapPreset;

            //Generate point for height discretion
            PlaneGenerator.discretePoints = Enumerable
                                           .Range(0, meshGeneratorPreset.perlinSettings.heightDiscretion + 1).ToList()
                                           .Select(t =>
                                                       t * meshGeneratorPreset.mapSettings.gridSize /
                                                       (float)meshGeneratorPreset.perlinSettings.heightDiscretion)
                                           .ToArray();

            float blockScale = 1;

            for (var i = 0; i < meshGeneratorPreset.mapSettings.materialResolution; i++)
            {
                blockScale /= 4f;
            }

            //Scale material according to the Grid Size 
            //Shader Forge
            meshGeneratorPreset.groundPrefab.GetComponent<Renderer>().sharedMaterial.SetTextureScale(
                "_EmissionTexture",
                new Vector2(blockScale / meshGeneratorPreset.mapSettings.gridSize,
                            blockScale / meshGeneratorPreset.mapSettings.gridSize));
            meshGeneratorPreset.groundPrefab.GetComponent<Renderer>().sharedMaterial.SetTextureScale(
                "_Texture",
                new Vector2(blockScale / meshGeneratorPreset.mapSettings.gridSize,
                            blockScale / meshGeneratorPreset.mapSettings.gridSize));

            //Create template plane
            _template = Instantiate(BuildTemplate());
        }

        private GameObject BuildTemplate()
        {
            //Create new map piece GameObject
            var newPart = Instantiate(meshGeneratorPreset.groundPrefab, _instance.transform);
            newPart.name = "Template";

            //Generate mesh
            var mesh = PlaneGenerator.GenerateQuadPlane(meshGeneratorPreset.mapSettings.mapWidth,
                                                        meshGeneratorPreset.mapSettings.mapHeight,
                                                        meshGeneratorPreset.mapSettings.gridSize,
                                                        meshGeneratorPreset.mapSettings.gridSize);

            //Assign produced mesh
            newPart.GetComponent<BakeMeshCollider>().meshToBake = mesh;

//            newPart.GetComponent<MeshFilter>().mesh = mesh;
//            newPart.GetComponent<MeshCollider>().sharedMesh = mesh;

            newPart.SetActive(false);

            return newPart;
        }

        private void CalculateSeed()
        {
            _seed = Random.value * Random.Range(-10000f, 10000f);
        }

        private static void BuildBasicMap()
        {
            MapPieces.Clear();

            foreach (Transform oldMapPiece in _instance.transform)
            {
                if (oldMapPiece != _instance.transform)
                {
                    Destroy(oldMapPiece.gameObject);
                }
            }

            for (var y = 0 - meshGeneratorPreset.mapSettings.blocksY / 2;
                 y <= meshGeneratorPreset.mapSettings.blocksY / 2;
                 y++)
            {
                for (var x = 0 - meshGeneratorPreset.mapSettings.blocksX / 2;
                     x <= meshGeneratorPreset.mapSettings.blocksX / 2;
                     x++)
                {
                    GenerateMapPieceDuplicate(x, y);
                }
            }
        }

        public static Tuple<int, int> PointToMapPieceLocation(Vector3 point)
        {
            int mapPieceX, mapPieceY;
            var mapWidth  = _instance.mapPreset.mapSettings.mapWidth  * _instance.mapPreset.mapSettings.gridSize;
            var mapHeight = _instance.mapPreset.mapSettings.mapHeight * _instance.mapPreset.mapSettings.gridSize;

            if (point.x < 0 && point.x > -mapWidth)
            {
                mapPieceX = -1;
            }
            else if (point.x >= 0)
            {
                mapPieceX = (int)point.x / mapWidth;
            }
            else
            {
                mapPieceX = (int)point.x / mapWidth - 1;
            }

            if (point.z < 0 && point.z > -mapHeight)
            {
                mapPieceY = -1;
            }
            else if (point.z >= 0)
            {
                mapPieceY = (int)point.z / mapHeight;
            }
            else
            {
                mapPieceY = (int)point.z / mapHeight - 1;
            }

            return new Tuple<int, int>(mapPieceX, mapPieceY);
        }

        public void FilledPixelCircle(int x0, int y0, int radius, CoordinateOperation function)
        {
            var x           = radius;
            var y           = 0;
            var xChange     = 1 - (radius << 1);
            var yChange     = 0;
            var radiusError = 0;

            while (x >= y)
            {
                for (var i = x0 - x; i <= x0 + x; i++)
                {
                    function(i, y0 + y);
                    function(i, y0 - y);
                }

                for (var i = x0 - y; i <= x0 + y; i++)
                {
                    function(i, y0 + x);
                    function(i, y0 - x);
                }

                y++;
                radiusError += yChange;
                yChange     += 2;
                if ((radiusError << 1) + xChange > 0)
                {
                    x--;
                    radiusError += xChange;
                    xChange     += 2;
                }
            }
        }

        public IEnumerator FilledPixelCircleOnePerFrame(int x0, int y0, int radius, CoordinateOperation function)
        {
            var x           = radius;
            var y           = 0;
            var xChange     = 1 - (radius << 1);
            var yChange     = 0;
            var radiusError = 0;

            while (x >= y)
            {
                for (var i = x0 - x; i <= x0 + x; i++)
                {
                    function(i, y0 + y);
//                    yield return new WaitForEndOfFrame();
                    function(i, y0 - y);
//                    yield return new WaitForEndOfFrame();
                }

                for (var i = x0 - y; i <= x0 + y; i++)
                {
                    function(i, y0 + x);
//                    yield return new WaitForEndOfFrame();
                    function(i, y0 - x);
//                    yield return new WaitForEndOfFrame();
                }

                y++;
                radiusError += yChange;
                yChange     += 2;
                if ((radiusError << 1) + xChange > 0)
                {
                    x--;
                    radiusError += xChange;
                    xChange     += 2;
                }

                yield return new WaitForEndOfFrame();
            }
        }

        private void DestroyFarMapPieces(int x, int y)
        {
            _currentPreloadCircle.Clear();

            FilledPixelCircle(x, y, meshGeneratorPreset.mapSettings.preloadRadius, AddTuple);

            MapPieces.Where(t => !_currentPreloadCircle.Contains(t.Key)).ForEach(t =>
            {
                Destroy(t.Value);
                MapPieces.Remove(t.Key);
            });
        }

        private static void PreloadNewMapPiece(int x, int y)
        {
            var index = Tuple.Create(x, y);
            if (!MapPieces.ContainsKey(index))
            {
                GenerateMapPieceDuplicate(x, y);
            }
        }

        private static void AddTuple(int x, int y)
        {
            _currentPreloadCircle.Add(new Tuple<int, int>(x, y));
        }

        public static void PreloadMapAtPoint(Vector3 point)
        {
            _instance.PreloadMapByMapPieceLocation(PointToMapPieceLocation(point));
        }

        public void PreloadMapByMapPieceLocation(Tuple<int, int> mapPieceLocation)
        {
            DestroyFarMapPieces(mapPieceLocation.Item1, mapPieceLocation.Item2);

            if (preloadBuffer != null)
            {
                StopCoroutine(preloadBuffer);
            }

            preloadBuffer = FilledPixelCircleOnePerFrame(mapPieceLocation.Item1, mapPieceLocation.Item2,
                                                         meshGeneratorPreset.mapSettings.preloadRadius,
                                                         PreloadNewMapPiece);

            StartCoroutine(preloadBuffer);

            if (RoadGenerator.instance)
            {
                RoadGenerator.instance.ExpandRoad();
            }

            if (RoadGeneratorGrid.instance)
            {
                RoadGeneratorGrid.instance.ExpandRoad();
            }
        }

        public static void GenerateMapPieceDuplicate(int x, int y)
        {
            var currentMapIndex = Tuple.Create(x, y);

            var perlinSettings = meshGeneratorPreset.perlinSettings;
            var mapSettings    = meshGeneratorPreset.mapSettings;

            if (MapPieces.ContainsKey(currentMapIndex))
            {
                throw new Exception($"Map piece with the same index exists! X:{x} Y:{y}");
            }

            MapPieces.Add(currentMapIndex, null);

            //Create new map piece GameObject
            var newPart = Instantiate(_template, _instance.transform);
            newPart.name = $"Terrain X:{x}, Y:{y}";
            newPart.transform.position =
                new Vector3(x * mapSettings.mapWidth  * mapSettings.gridSize, 0f,
                            y * mapSettings.mapHeight * mapSettings.gridSize);

            var mesh = Instantiate(newPart.GetComponent<BakeMeshCollider>().meshToBake);
//            var mesh = newPart.GetComponent<MeshFilter>().mesh;


            //Set mesh's vertex heights

            if (meshGeneratorPreset.perlinSettings.usePerlin)
            {
                PlaneGenerator.SetPerlinHeights(ref mesh,
                                                x * mapSettings.mapWidth  * mapSettings.gridSize,
                                                y * mapSettings.mapHeight * mapSettings.gridSize,
                                                perlinSettings.perlinMultiplier,
                                                perlinSettings.perlinDivisionX, perlinSettings.perlinDivisionY,
                                                perlinSettings.heightThreshold, _seed, perlinSettings.heightDiscretion,
                                                mapSettings.gridSize);
            }

            //Assign produced mesh
//            newPart.GetComponent<MeshFilter>().mesh = mesh;
            newPart.GetComponent<BakeMeshCollider>().meshToBake = mesh;
//            newPart.GetComponent<BakeMeshCollider>().PreBake(mesh);

//            newPart.GetComponent<MeshCollider>().sharedMesh = mesh;

            //Set the index of the map piece
            var preloadTrigger = newPart.GetComponent<PreloadTrigger>();
            preloadTrigger.x = x;
            preloadTrigger.y = y;

            //Set the gameObject to the dictionary
            MapPieces[currentMapIndex] = newPart;


            if (meshGeneratorPreset.SpawnObjects.Count > 0)
            {
                for (var i = 0; i < meshGeneratorPreset.spawnObjectsNumber; i++)
                {
                    var spawnObject = meshGeneratorPreset.SpawnObjects.GetRandom();
                    var newObject   = Instantiate(spawnObject, newPart.transform).transform;
                    newObject.position =
                        new Vector3(
                            Random.Range(
                                x * meshGeneratorPreset.mapSettings.mapWidth *
                                meshGeneratorPreset.mapSettings.gridSize,
                                (x + 1) * meshGeneratorPreset.mapSettings.mapWidth *
                                meshGeneratorPreset.mapSettings.gridSize), 0f,
                            Random.Range(
                                y * meshGeneratorPreset.mapSettings.mapWidth *
                                meshGeneratorPreset.mapSettings.gridSize,
                                (y + 1) * meshGeneratorPreset.mapSettings.mapWidth *
                                meshGeneratorPreset.mapSettings.gridSize));
                    var rotation = newObject.rotation;
                    rotation = Quaternion.Euler(rotation.eulerAngles.x, Random.Range(0, 361),
                                                rotation.eulerAngles.z);
                    newObject.rotation = rotation;

                    if (spawnObject.name == "Pyramid")
                    {
                        var position = newObject.position;
                        var closestGridPoint =
                            new Vector3(
                                Mathf.Round(position.x / meshGeneratorPreset.mapSettings.gridSize) *
                                meshGeneratorPreset.mapSettings.gridSize, 0f,
                                Mathf.Round(position.z / meshGeneratorPreset.mapSettings.gridSize) *
                                meshGeneratorPreset.mapSettings.gridSize);

                        for (var q = -4; q <= 4; q++)
                        {
                            for (var j = -4; j < 4; j++)
                            {
                                VertexAccess.SetWorldVertexHeightAtPoint(
                                    closestGridPoint.x + j * meshGeneratorPreset.mapSettings.gridSize,
                                    closestGridPoint.z + q * meshGeneratorPreset.mapSettings.gridSize,
                                    0f);
                            }
                        }
                    }
                }
            }

            newPart.SetActive(true);
        }
    }
}