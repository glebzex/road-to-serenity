﻿using System;
using UnityEngine;

namespace WorldGeneration
{
    public class PreloadTrigger : MonoBehaviour
    {
        public int x, y;


        private void OnTriggerEnter(Collider other1)
        {
            Preload();
        }

        public void Preload()
        {
            WorldGenerator._instance.PreloadMapByMapPieceLocation(new Tuple<int, int>(x, y));
        }
    }
}