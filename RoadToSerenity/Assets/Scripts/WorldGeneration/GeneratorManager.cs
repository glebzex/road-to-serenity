﻿using System;
using System.Linq;
using UnityEngine;
using WorldGeneration;

public class GeneratorManager : MonoBehaviour
{
    private static bool       _loadNew;
    public         GameObject roadsWeb;
    public         GameObject roadsGrid;
    public         GameObject meshGenerator;

    [SerializeField]
    private RealmPreset[] realmPresets;

    private RealmPreset _realmPreset;

    private void Awake()
    {
        if (_loadNew && _realmPreset != null)
        {
            _realmPreset = realmPresets.Where(t => t != _realmPreset).RandomElement();
            _loadNew     = false;
        }
        else
        {
            _realmPreset = realmPresets.RandomElement();
        }


        var skybox = _realmPreset.Skybox.RandomElement();
        if (skybox == null)
        {
            Debug.LogError($"No Skybox in Realm Preset {_realmPreset.name} found!");
        }
        else
        {
            RenderSettings.skybox = _realmPreset.Skybox.RandomElement();
        }

        switch (_realmPreset.RoadsPreset.RoadType)
        {
            case RoadType.Web:
                roadsWeb.transform.GetComponentInChildren<RoadGenerator>().roadWebPreset =
                    _realmPreset.RoadsPreset.RoadWebPreset;

                roadsWeb.SetActive(true);
                roadsGrid.SetActive(false);
                break;
            case RoadType.Grid:
                roadsGrid.transform.GetComponentInChildren<RoadGeneratorGrid>().roadGridPreset =
                    _realmPreset.RoadsPreset.RoadGridPreset;

                roadsWeb.SetActive(false);
                roadsGrid.SetActive(true);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        meshGenerator.GetComponent<WorldGenerator>().mapPreset = _realmPreset.MeshGeneratorPreset;
        meshGenerator.SetActive(true);
    }

    public static void LoadDifferentRealmNextStart()
    {
        _loadNew = true;
    }
}