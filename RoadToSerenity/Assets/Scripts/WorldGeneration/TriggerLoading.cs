﻿using UnityEngine;
using WorldGeneration;

public class TriggerLoading : MonoBehaviour
{
    private PreloadTrigger _cachePreloadTrigger;
    private RaycastHit     _cacheRaycastHit;
    private Transform      _cacheTransform;

    public RaycastHit raycastHit => _cacheRaycastHit;

    private void FixedUpdate()
    {
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 5f, transform.position.z),
                            Vector3.down,
                            out _cacheRaycastHit, Mathf.Infinity,
                            LayerMask.GetMask("Ground")))
        {
            if (_cacheTransform == _cacheRaycastHit.transform)
            {
                return;
            }

            _cacheTransform      = _cacheRaycastHit.transform;
            _cachePreloadTrigger = _cacheRaycastHit.transform.GetComponent<PreloadTrigger>();
            if (_cachePreloadTrigger)
            {
                _cachePreloadTrigger.Preload();
            }
        }
    }
}