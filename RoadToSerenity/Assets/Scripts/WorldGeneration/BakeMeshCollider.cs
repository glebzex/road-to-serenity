﻿using System.Threading.Tasks;
using UnityEngine;

public class BakeMeshCollider : MonoBehaviour
{
    public Mesh meshToBake;

    private MeshCollider _meshCollider;

    private volatile bool _isBaked;

    private void Awake()
    {
        _meshCollider = gameObject.AddComponent<MeshCollider>();

        GetComponent<MeshFilter>().mesh = meshToBake;

        var meshId = meshToBake.GetInstanceID();

        Task.Run(delegate
        {
            Physics.BakeMesh(meshId, false);
            _isBaked = true;
        });
    }

    private void Update()
    {
        if (_isBaked)
        {
            _meshCollider.sharedMesh = meshToBake;
            Destroy(this);
        }
    }
}