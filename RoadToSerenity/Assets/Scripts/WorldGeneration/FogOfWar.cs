﻿using System.Collections;
using UnityEngine;
using WorldGeneration;

public class FogOfWar : MonoBehaviour
{
    public GameObject particlePrefab;

    [Tooltip("Spawn a particle each \"Spawn Angle\"")]
    public int spawnAngle = 2;

    public float particleScale = 100f;

    public float yOffset;

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(nameof(WaitAndGenerate));
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position =
            new Vector3(CarController.CarTransform.position.x, yOffset, CarController.CarTransform.position.z);
    }

    private IEnumerator WaitAndGenerate()
    {
        var limiter = 0;
        while (WorldGenerator.meshGeneratorPreset == null && limiter < 20)
        {
            limiter++;
            yield return new WaitForEndOfFrame();
        }

        var radius = WorldGenerator.meshGeneratorPreset.mapSettings.preloadRadius *
            WorldGenerator.meshGeneratorPreset.mapSettings.mapHeight              *
            WorldGenerator.meshGeneratorPreset.mapSettings.gridSize;
        for (var i = 0; i < 360 / spawnAngle; i++)
        {
            var newPiece = Instantiate(particlePrefab,
                                       new Vector3(radius * Mathf.Cos(i * spawnAngle * Mathf.Deg2Rad), yOffset,
                                                   radius * Mathf.Sin(i * spawnAngle * Mathf.Deg2Rad)),
                                       Quaternion.identity).transform;
            newPiece.localScale *= particleScale;
            newPiece.transform.SetParent(transform);
        }
    }
}