using System;
using UnityEngine;

namespace WorldGeneration
{
    public static class VertexAccess
    {
        private static float newX,      newY;
        private static int   mapPieceX, mapPieceY;

        private static readonly int mapWidth = WorldGenerator.meshGeneratorPreset.mapSettings.mapWidth *
            WorldGenerator.meshGeneratorPreset.mapSettings.gridSize;

        private static readonly int mapHeight = WorldGenerator.meshGeneratorPreset.mapSettings.mapHeight *
            WorldGenerator.meshGeneratorPreset.mapSettings.gridSize;

        private static Tuple<int, int> tuplePosition;
        private static int             vertexCount;

        private static MeshData _meshData;

        private static MeshData getMeshTemp;

        public static Vector3 ClosestMapPoint(float x, float y)
        {
            return new Vector3(
                Mathf.Round(x / WorldGenerator.meshGeneratorPreset.mapSettings.gridSize) *
                WorldGenerator.meshGeneratorPreset.mapSettings.gridSize, 0f,
                Mathf.Round(y / WorldGenerator.meshGeneratorPreset.mapSettings.gridSize) *
                WorldGenerator.meshGeneratorPreset.mapSettings.gridSize);
        }

        public static Vector3 ClosestMapPoint(Vector3 originalPoint)
        {
            return new Vector3(
                Mathf.Round(originalPoint.x / WorldGenerator.meshGeneratorPreset.mapSettings.gridSize) *
                WorldGenerator.meshGeneratorPreset.mapSettings.gridSize, 0f,
                Mathf.Round(originalPoint.z / WorldGenerator.meshGeneratorPreset.mapSettings.gridSize) *
                WorldGenerator.meshGeneratorPreset.mapSettings.gridSize);
        }


        public static void SetWorldVertexHeightAtPoint(float vertexCoordinateX, float vertexCoordinateY,
                                                       float newVertexHeight)
        {
            if (vertexCoordinateX % WorldGenerator.meshGeneratorPreset.mapSettings.gridSize != 0 ||
                vertexCoordinateY % WorldGenerator.meshGeneratorPreset.mapSettings.gridSize != 0)
            {
                return;
            }

            mapPieceX = 0;
            mapPieceY = 0;
            newX      = vertexCoordinateX;
            newY      = vertexCoordinateY;

            (mapPieceX, mapPieceY) = WorldGenerator.PointToMapPieceLocation(new Vector3(newX, 0f, newY));


            if (newX < 0 && newY >= 0)
            {
                newX =  mapWidth + newX % mapWidth;
                newY %= mapHeight;
            }

            else if (newX >= 0 && newY < 0)
            {
                newX %= mapWidth;
                newY =  mapHeight + newY % mapHeight;
            }

            else if (newX < 0 && newY < 0)
            {
                newX = mapWidth  + newX % mapWidth;
                newY = mapHeight + newY % mapHeight;
            }

            else if (newX >= 0 && newY >= 0)
            {
                newX %= mapWidth;
                newY %= mapHeight;
            }


//            Debug.Log($"{mapPieceX}:{mapPieceY}  ({vertexCoordinateX}, {vertexCoordinateY}):({newX}, {newY})");


            if (newX == 0f)
            {
                SetHeightByMapCoordinate(mapPieceX - 1, mapPieceY, mapWidth, newY, newVertexHeight);
            }

            if (newY == 0f)
            {
                SetHeightByMapCoordinate(mapPieceX, mapPieceY - 1, newX, mapHeight, newVertexHeight);
            }

            if (newX == mapWidth)
            {
                SetHeightByMapCoordinate(mapPieceX + 1, mapPieceY, 0, newY, newVertexHeight);
            }

            if (newY == mapHeight)
            {
                SetHeightByMapCoordinate(mapPieceX, mapPieceY + 1, newX, 0, newVertexHeight);
            }

            if (newX == 0f && newY == 0f)
            {
                SetHeightByMapCoordinate(mapPieceX - 1, mapPieceY - 1, mapWidth, mapHeight, newVertexHeight);
            }

            if (newX == 0f && newY == mapHeight)
            {
                SetHeightByMapCoordinate(mapPieceX - 1, mapPieceY + 1, mapWidth, 0, newVertexHeight);
            }

            if (newX == mapWidth && newY == 0f)
            {
                SetHeightByMapCoordinate(mapPieceX + 1, mapPieceY - 1, 0, mapHeight, newVertexHeight);
            }

            if (newX == mapWidth && newY == mapHeight)
            {
                SetHeightByMapCoordinate(mapPieceX + 1, mapPieceY + 1, 0, 0, newVertexHeight);
            }

            SetHeightByMapCoordinate(mapPieceX, mapPieceY, newX, newY, newVertexHeight);
        }

        private static void SetHeightByMapCoordinate(int   xPositionMap,    int   yPositionMap, float xPositionVertex,
                                                     float yPositionVertex, float height)
        {
            tuplePosition = new Tuple<int, int>(xPositionMap, yPositionMap);

            var any = false;
            foreach (var t in WorldGenerator.MapPieces.Keys)
            {
                if (t.Item1 == xPositionMap && t.Item2 == yPositionMap)
                {
                    any = true;
                    break;
                }
            }

            if (!any)
            {
//                Debug.LogWarning($"No vertex rendered at point {tuple.Item1}, {tuple.Item2}");
                return;
            }

            _meshData = GetMeshData(tuplePosition);

            vertexCount = 0;
            for (var i = 0; i < _meshData.vertices.Length; i++)
            {
                //Normally there shouldn't be more than 4 vertices at the same point 
                if (vertexCount >= 4)
                {
                    break;
                }

                if (_meshData.vertices[i].x == xPositionVertex && _meshData.vertices[i].z == yPositionVertex)
                {
                    _meshData.vertices[i].y = height;
                    vertexCount++;
                }
            }

            QueueGeneration.QueueMesh(tuplePosition);
        }

        private static MeshData GetMeshData(Tuple<int, int> meshPosition)
        {
            getMeshTemp = null;
            for (var index = 0; index < QueueGeneration.MeshList.Count; index++)
            {
                var t = QueueGeneration.MeshList[index];
                if (t.index.Item1 == meshPosition.Item1 && t.index.Item2 == meshPosition.Item2)
                {
                    getMeshTemp = t;
                    break;
                }
            }

            if (getMeshTemp != null)
            {
                return getMeshTemp;
            }

            var meshFilter = WorldGenerator.MapPieces[meshPosition].GetComponent<MeshFilter>();

            getMeshTemp = new MeshData
            {
                index        = meshPosition,
                mesh         = meshFilter.mesh,
                meshCollider = meshFilter.transform.GetComponent<MeshCollider>(),
                meshFilter   = meshFilter,
                vertices     = meshFilter.mesh.vertices
            };
            QueueGeneration.MeshList.Add(getMeshTemp);

            return getMeshTemp;
        }
    }
}