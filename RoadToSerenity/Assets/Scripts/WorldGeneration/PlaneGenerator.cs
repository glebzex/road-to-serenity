﻿using UnityEngine;

namespace WorldGeneration
{
    public static class PlaneGenerator
    {
        public static float[] discretePoints;

        public static void SetPerlinHeights(ref Mesh mesh, float xOffset = 0, float yOffset = 0,
                                            float    perlinMultiplier = 10f,
                                            float    perlinDivisionX  = 15f, float perlinDivisionY = 15f,
                                            float    heightThreshold  = 0f,  float seed            = 0f,
                                            int      yDivisions       = 0,   float gridSize        = 1f)
        {
            var   vertices               = mesh.vertices;
            var   currentVertex          = new Vector3(0f, 0f, 0f);
            var   closestFlooredMultiple = 0f;
            float vertexHeight;
            float heightFraction;

            for (var i = 0; i < vertices.Length; i++)
            {
                currentVertex.x = vertices[i].x;
                currentVertex.z = vertices[i].z;

                vertexHeight = Mathf.PerlinNoise((currentVertex.x + xOffset) / perlinDivisionX + seed,
                                                 (currentVertex.z + yOffset) / perlinDivisionY + seed);

                vertexHeight =  vertexHeight >= heightThreshold ? vertexHeight : heightThreshold;
                vertexHeight *= perlinMultiplier;
                vertexHeight -= heightThreshold * perlinMultiplier;


                if (yDivisions != 0)
                {
                    closestFlooredMultiple = Mathf.Floor(vertexHeight / gridSize) * gridSize;

                    heightFraction = Mathf.Abs(vertexHeight - closestFlooredMultiple);
                    int l = 0, r = discretePoints.Length - 1;
                    var m = 0;
                    while (l <= r)
                    {
                        m = (l + r) / 2;
                        if (discretePoints[m] < heightFraction)
                        {
                            l = m + 1;
                        }
                        else if (discretePoints[m] > heightFraction)
                        {
                            r = m - 1;
                        }
                        else
                        {
                            break;
                        }
                    }

                    heightFraction = discretePoints[m];
                    vertices[i] = new Vector3(currentVertex.x, closestFlooredMultiple + heightFraction,
                                              currentVertex.z);
                }
                else
                {
                    vertices[i] = new Vector3(currentVertex.x, vertexHeight, currentVertex.z);
                }
            }

            mesh.vertices = vertices;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }

        /// <summary>
        ///     Generates a quad mesh assigning one to four vertices to one geometrical vertex.
        ///     (Depends on number of adjacent faces)
        ///     Contains UV, triangles, tangents
        /// </summary>
        /// <param name="mapWidth">Plane Width</param>
        /// <param name="mapHeight">Plane Height</param>
        /// <returns>Mesh instance</returns>
        public static Mesh GenerateQuadPlane(int mapWidth, int mapHeight, float blockSizeX = 1f, float blockSizeY = 1f,
                                             float gridPositionX = 0, float gridPositionY = 0)
        {
            var mesh = new Mesh { name = "Procedural Grid" };

            //TODO: Manually calculate normals

            mesh.vertices = GenerateSplitVertexArray(mapWidth, mapHeight, blockSizeX, blockSizeY, gridPositionX,
                                                     gridPositionY);

            mesh.uv = CalculateQuadUv(mesh.vertices);
//            mesh.tangents = tangents;
            mesh.triangles = CalculateTrianglesMultiVertex(mapWidth, mapHeight);

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }

        /// <summary>
        ///     Generates a quad mesh assigning only one rendering vertex to one geometrical vertex.
        ///     Contains UV, triangles, tangents
        /// </summary>
        /// <param name="mapWidth">Plane Width</param>
        /// <param name="mapHeight">Plane Height</param>
        /// <returns>Mesh instance</returns>
        public static Mesh GenerateQuadPlaneSoftEdges(int   mapWidth, int mapHeight, int xOffset = 0, int yOffset = 0,
                                                      float blockSizeX = 1f, float blockSizeY = 1f)
        {
            var mesh = new Mesh { name = "Procedural Grid" };

            var vertices = new Vector3[(mapWidth + 1) * (mapHeight + 1)];
            var tangents = new Vector4[vertices.Length];
            var tangent  = new Vector4(1f, 0f, 0f, -1f);

            //Calculate vertices
            for (int i = 0, z = 0; z <= mapHeight; z++)
            {
                for (var x = 0; x <= mapWidth; x++)
                {
                    vertices[i] = new Vector3(x * blockSizeX, 0f, z * blockSizeY);
                    tangents[i] = tangent;
                    i++;
                }
            }

            //Fill the instance
            mesh.vertices  = GenerateSplitVertexArray(mapWidth, mapHeight);
            mesh.uv        = CalculateQuadUv(mesh.vertices);
            mesh.tangents  = tangents;
            mesh.triangles = CalculateTrianglesOneVertex(mapWidth, mapHeight);

            mesh.RecalculateNormals();

            return mesh;
        }

        private static Vector2[] CalculateQuadUv(Vector3[] vertices)
        {
            var uv = new Vector2[vertices.Length];

            for (var i = 0; i < uv.Length; i++)
            {
                uv[i] = new Vector2(vertices[i].x, vertices[i].z);
            }

            return uv;
        }

        private static Vector3[] GenerateSplitVertexArray(int   mapWidth, int mapHeight, float blockSizeX = 1f,
                                                          float blockSizeY    = 1f, float gridPositionX = 0,
                                                          float gridPositionY = 0)
        {
            //First (Last) Row Vertex Count
            var frvc = 2 + 2 * (mapWidth - 1);
            //Row (Middle) Vertex Count
            var rvc = 4 + (mapWidth - 1) * 4;

            var vertices = new Vector3[2 * frvc + (mapHeight - 1) * rvc];

            var tangents = new Vector4[vertices.Length];
            var tangent  = new Vector4(1f, 0f, 0f, -1f);

            var newVertex = new Vector3(0f, 0f, 0f);
            for (int y = 0, vertexCounter = 0; y <= mapHeight; y++)
            {
                newVertex.z = gridPositionY + y * blockSizeY;
                for (var x = 0; x <= mapWidth; x++)
                {
                    newVertex.x = gridPositionX + x * blockSizeX;
                    if ((x == 0        && y == 0) || (x == 0) & (y == mapHeight) || (x == mapWidth && y == 0) ||
                        (x == mapWidth && y == mapHeight))
                    {
                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;
                        continue;
                    }

                    if (x == 0 || y == 0 || x == mapWidth || y == mapHeight)
                    {
                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;

                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;
                    }
                    else
                    {
                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;

                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;

                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;

                        tangents[vertexCounter] = tangent;
                        vertices[vertexCounter] = newVertex;
                        vertexCounter++;
                    }
                }
            }

            return vertices;
        }

        private static int[] CalculateTrianglesOneVertex(int mapWidth, int mapHeight)
        {
            var triangles = new int[mapWidth * mapHeight * 6];
            for (int ti = 0, vi = 0, y = 0; y < mapHeight; y++, vi++)
            {
                for (var x = 0; x < mapWidth; x++, ti += 6, vi++)
                {
                    triangles[ti]     = vi;
                    triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                    triangles[ti + 4] = triangles[ti + 1] = vi + mapWidth + 1;
                    triangles[ti + 5] = vi + mapWidth + 2;
                }
            }

            return triangles;
        }

        private static int[] CalculateTrianglesMultiVertex(int mapWidth, int mapHeight)
        {
            //First (Last) Row Vertex Count
            var frvc = 2 + 2 * (mapWidth - 1);
            //Row (Middle) Vertex Count
            var rvc = 4 + (mapWidth - 1) * 4;

            var vertexCount = 2 * frvc + (mapHeight - 1) * rvc;

            //Calculate triangles
            var triangles = new int[mapWidth * mapHeight * 6];
            int i         = 0, triIndex = 0;

            //First row first quad
            triangles[triIndex++] = i;
            triangles[triIndex++] = i + frvc;
            triangles[triIndex++] = i + 1;

            triangles[triIndex++] = i + 1;
            triangles[triIndex++] = i + frvc;
            triangles[triIndex++] = i + frvc + 2;


            //First row second+ quads
            for (var t = i + 2; t < frvc - 1; t += 2)
            {
                triangles[triIndex++] = t;
                triangles[triIndex++] = frvc + 3 + (t - 2) * 2;
                triangles[triIndex++] = t    + 1;

                triangles[triIndex++] = t    + 1;
                triangles[triIndex++] = frvc + 3 + (t - 2) * 2;
                triangles[triIndex++] = frvc + 6 + (t - 2) * 2;
            }

            i += frvc + 1;

            for (;;)
            {
                triangles[triIndex++] = i;
                triangles[triIndex++] = i + rvc - 1;
                triangles[triIndex++] = i       + 3;

                triangles[triIndex++] = i       + 3;
                triangles[triIndex++] = i + rvc - 1;
                triangles[triIndex++] = i       + rvc + 1;

                for (var p = i + 4; p < i + rvc - 7; p += 4)
                {
                    triangles[triIndex++] = p;
                    triangles[triIndex++] = p + rvc - 2;
                    triangles[triIndex++] = p       + 3;

                    triangles[triIndex++] = p       + 3;
                    triangles[triIndex++] = p + rvc - 2;
                    triangles[triIndex++] = p       + rvc + 1;
                }

                var m = i + rvc - 4;

                triangles[triIndex++] = m;
                triangles[triIndex++] = m + rvc - 2;
                triangles[triIndex++] = m       + 2;

                triangles[triIndex++] = m       + 2;
                triangles[triIndex++] = m + rvc - 2;
                triangles[triIndex++] = m       + rvc + 1;

                if (i + rvc < vertexCount - frvc - rvc)
                {
                    i += rvc;
                }
                else
                {
                    i += rvc;
                    break;
                }
            }


            triangles[triIndex++] = i;
            triangles[triIndex++] = i + rvc - 1;
            triangles[triIndex++] = i       + 3;

            triangles[triIndex++] = i       + 3;
            triangles[triIndex++] = i + rvc - 1;
            triangles[triIndex++] = i       + rvc;


            for (var q = i + 4; q < vertexCount - frvc - 6; q += 4)
            {
                triangles[triIndex++] = q;
                triangles[triIndex++] = vertexCount - frvc + 2 + (q - i - 4) / 2;
                triangles[triIndex++] = q                  + 1;

                triangles[triIndex++] = q                  + 3;
                triangles[triIndex++] = vertexCount - frvc + 2 + (q - i - 4) / 2;
                triangles[triIndex++] = vertexCount - frvc + 3 + (q - i - 4) / 2;
            }

            i += rvc - 4;

            triangles[triIndex++] = i;
            triangles[triIndex++] = vertexCount - 2;
            triangles[triIndex++] = i           + 2;

            triangles[triIndex++] = i           + 2;
            triangles[triIndex++] = vertexCount - 2;
            triangles[triIndex]   = vertexCount - 1;

            return triangles;
        }
    }
}