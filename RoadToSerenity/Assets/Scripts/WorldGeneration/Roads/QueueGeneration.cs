﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshData
{
    public Tuple<int, int> index;
    public bool            isAwaiting;
    public Mesh            mesh;
    public MeshCollider    meshCollider;
    public MeshFilter      meshFilter;
    public float           queueTimer;
    public Vector3[]       vertices;
}

public class QueueGeneration : MonoBehaviour
{
    public static readonly List<MeshData> MeshList = new();

    public static QueueGeneration Instance;

    public float awaitingCooldown = 5f;

    private void Awake()
    {
        Instance = this;
        MeshList.Clear();
    }

    public static void QueueMesh(Tuple<int, int> meshPosition)
    {
        var meshData = MeshList.Find(t => t.index.Item1 == meshPosition.Item1 && t.index.Item2 == meshPosition.Item2);

        if (meshData != null && Instance != null)
        {
            Instance.StartCoroutine(Instance.WaitAndRecalculate(meshData));
        }
    }

    private IEnumerator WaitAndRecalculate(MeshData meshData)
    {
        meshData.queueTimer = 0f;

        if (meshData.isAwaiting)
        {
            yield break;
        }

        meshData.isAwaiting = true;

        while (meshData.queueTimer <= awaitingCooldown)
        {
            meshData.queueTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        if (meshData.meshFilter == null)
        {
            yield break;
        }

        meshData.mesh.vertices = meshData.vertices;

        meshData.meshFilter.mesh = meshData.mesh;
//        meshData.meshCollider.sharedMesh = meshData.mesh;
        meshData.mesh.RecalculateNormals();

        meshData.isAwaiting = false;
    }
}