﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WorldGeneration
{
    public class RoadGeneratorGrid : MonoBehaviour
    {
        public static RoadGeneratorGrid instance;
        public        RoadGridPreset    roadGridPreset;

        private readonly List<RoadPiece> _roadPieces = new();


        private bool _isFillingMap;

        private Transform _roadParent;

        private void Awake()
        {
            instance = this;

            //Check if all the prerequisites are set
            if (!(roadGridPreset.fourWayRoad.Length == 0 || roadGridPreset.straightRoad == null))
            {
                GenerateFirstRoad();
            }
        }

        public void ExpandRoad()
        {
            StartCoroutine(nameof(FillMapWithRoads));
        }

        private IEnumerator FillMapWithRoads()
        {
            if (_isFillingMap)
            {
                yield break;
            }

            _isFillingMap = true;

            yield return new WaitForEndOfFrame();
            var limiter = 500;
            while (IterateRoads() > 0 && limiter > 0)
            {
                limiter--;
                yield return new WaitForEndOfFrame();
            }

            _isFillingMap = false;
            if (limiter == 0)
            {
                Debug.LogWarning("Hit the limit of loops for road generation!");
            }
        }

        private int IterateRoads()
        {
            var count = 0;
            _roadPieces.Where(t => t.hasEmptySnapPoints).ForEach(t => count += FillRoadPiece(t));
            return count;
        }

        private void GenerateFirstRoad()
        {
            _roadParent = new GameObject("Road Parent").transform;

            var newRoadPiece = new RoadPiece(roadGridPreset.fourWayRoad.RandomElement());
            newRoadPiece.gameObject.transform.SetParent(_roadParent.transform);
            _roadParent.Translate(Vector3.down * roadGridPreset.yOffset);
            _roadPieces.Add(newRoadPiece);
            FillRoadPiece(newRoadPiece);
        }

        private int FillRoadPiece(RoadPiece current)
        {
            var count = 0;
            foreach (var snapPoint in current.snapPoints)
            {
                if (snapPoint.isOccupied)
                {
                    continue;
                }

                var currentPosCenter = current.gameObject.transform.position;
                if (!Physics.Raycast(new Vector3(currentPosCenter.x, currentPosCenter.y + 1000f, currentPosCenter.z),
                                     Vector3.down, out var hit, 1500f, LayerMask.GetMask("Ground")))
                {
                    continue;
                }

                var cols = new Collider[6];
                Physics.OverlapSphereNonAlloc(snapPoint.pointTransform.position, 3f, cols, LayerMask.GetMask("Road"));
                if (cols.Count(t => t != null && t.transform.parent != snapPoint.pointTransform.parent) > 3)
                {
                    snapPoint.isOccupied = true;
                    continue;
                }

                GameObject newRoadPiece = null;

                var roadNumber = 0;
                if (int.TryParse(current.gameObject.name, out roadNumber) && roadNumber == 0)
                {
                    newRoadPiece = roadGridPreset.fourWayRoad.RandomElement();
                }
                else if (roadNumber > 0)
                {
                    newRoadPiece      = Instantiate(roadGridPreset.straightRoad);
                    newRoadPiece.name = (roadNumber - 1).ToString();
                }
                else
                {
                    newRoadPiece      = Instantiate(roadGridPreset.straightRoad);
                    newRoadPiece.name = roadGridPreset.gridSize.ToString();
                }

                var newSnapRoadPiece = new RoadPiece(newRoadPiece);
                current.SnapToRoadPiece(newSnapRoadPiece);
                newSnapRoadPiece.gameObject.transform.SetParent(_roadParent);

                var closestGridPoint = VertexAccess.ClosestMapPoint(newSnapRoadPiece.gameObject.transform.position);

                for (var i = -2; i <= 2; i++)
                {
                    for (var j = -2; j < 2; j++)
                    {
                        VertexAccess.SetWorldVertexHeightAtPoint(
                            closestGridPoint.x + j * WorldGenerator.meshGeneratorPreset.mapSettings.gridSize,
                            closestGridPoint.z + i * WorldGenerator.meshGeneratorPreset.mapSettings.gridSize, 0f);
                    }
                }

                _roadPieces.Add(newSnapRoadPiece);

                count++;
            }

            return count;
        }
    }
}