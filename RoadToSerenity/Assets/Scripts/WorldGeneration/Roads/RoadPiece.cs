using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WorldGeneration
{
    public class RoadPiece
    {
        public readonly GameObject      gameObject;
        public readonly List<SnapPoint> snapPoints = new();

        public RoadPiece(GameObject prefab)
        {
            if (prefab.scene == SceneManager.GetActiveScene())
            {
                gameObject = prefab;
            }
            else
            {
                gameObject = Object.Instantiate(prefab);
            }

            foreach (Transform child in gameObject.transform)
            {
                if (child.name == "Snap Points")
                {
                    foreach (Transform point in child)
                    {
                        if (point != child)
                        {
                            snapPoints.Add(new SnapPoint(point));
                        }
                    }

                    break;
                }
            }

            if (snapPoints.Count == 0)
            {
                Debug.LogWarning("No snap points found!", gameObject);
            }
        }

        public bool hasEmptySnapPoints => snapPoints.Exists(t => t.isOccupied == false);

        public void SnapToRoadPiece(RoadPiece other)
        {
            var emptySnapPoint = snapPoints.FirstOrDefault(t => !t.isOccupied);
            if (emptySnapPoint == null)
            {
                Debug.LogError("No un-occupied snap point found on the road piece to snap to!");
                return;
            }

            var otherSnapPoint = other.snapPoints.FirstOrDefault();
            if (otherSnapPoint == null)
            {
                Debug.LogError("No un-occupied snap point found on other road piece to snap to!");
                return;
            }


            emptySnapPoint.isOccupied = true;
            otherSnapPoint.isOccupied = true;

            var originalParent = otherSnapPoint.pointTransform.parent;
            otherSnapPoint.pointTransform.parent = null;
            other.gameObject.transform.SetParent(otherSnapPoint.pointTransform);
            otherSnapPoint.pointTransform.transform.SetParent(emptySnapPoint.pointTransform, false);
            otherSnapPoint.pointTransform.localPosition = Vector3.zero;
            otherSnapPoint.pointTransform.localRotation = Quaternion.identity;
            otherSnapPoint.pointTransform.Rotate(Vector3.up, 180f);

            other.gameObject.transform.parent = null;

            otherSnapPoint.pointTransform.SetParent(originalParent);
        }
    }
}