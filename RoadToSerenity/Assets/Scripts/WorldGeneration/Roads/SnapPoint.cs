using UnityEngine;

namespace WorldGeneration
{
    public class SnapPoint
    {
        public readonly Transform pointTransform;
        public          bool      isOccupied;

        public SnapPoint(Transform pointTransform, bool isOccupied = false)
        {
            this.pointTransform = pointTransform;
            this.isOccupied     = isOccupied;
        }
    }
}