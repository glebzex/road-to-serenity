﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils;

namespace WorldGeneration
{
    public class RoadGenerator : MonoBehaviour
    {
        public static RoadGenerator instance;
        public        RoadWebPreset roadWebPreset;

        private readonly List<RoadPiece> _roadPieces = new();
        private          Transform       _roadParent;

        private WeightedRandom<GameObject> _roadPrefabRandomList;

        private bool isFillingMap;


        private Vector3 lastCityPoint = Vector3.zero;

        private void Awake()
        {
            instance = this;

            _roadPrefabRandomList = new WeightedRandom<GameObject>(roadWebPreset.roadPrefabs);

            GenerateFirstRoad();
        }

        public void ExpandRoad()
        {
            StartCoroutine(nameof(FillMapWithRoads));
        }

        private IEnumerator FillMapWithRoads()
        {
            if (isFillingMap)
            {
                yield break;
            }

            isFillingMap = true;

            yield return new WaitForEndOfFrame();
            var limiter = 500;
            while (IterateRoads() > 0 && limiter > 0)
            {
                limiter--;
//                yield return new WaitForSeconds(0.3f);
                yield return new WaitForEndOfFrame();
            }

            isFillingMap = false;
            if (limiter == 0)
            {
                Debug.LogWarning("Hit the limit of loops for road generation!");
            }
        }

        private int IterateRoads()
        {
            var count = 0;
            _roadPieces.Where(t => t.hasEmptySnapPoints).ForEach(t => count += FillRoadPiece(t));
            return count;
        }

        private void GenerateFirstRoad()
        {
            _roadParent = new GameObject("Road Parent").transform;

            //Hardcoded 4-way
            var newRoadPiece = new RoadPiece(roadWebPreset.roadPrefabs[3].GetItem());
            newRoadPiece.gameObject.transform.SetParent(_roadParent.transform);
            _roadParent.Translate(Vector3.down * roadWebPreset.yOffset);
            _roadPieces.Add(newRoadPiece);
            FillRoadPiece(newRoadPiece);
        }

        private int FillRoadPiece(RoadPiece current)
        {
            var count = 0;
            foreach (var snapPoint in current.snapPoints)
            {
                if (snapPoint.isOccupied)
                {
                    continue;
                }

                var currentPosCenter = current.gameObject.transform.position;

                if (!Physics.Raycast(new Vector3(currentPosCenter.x, currentPosCenter.y + 1000f, currentPosCenter.z),
                                     Vector3.down, out var hit, 1500f,
                                     LayerMask.GetMask("Ground")))
                {
                    continue;
                }

                var cols = new Collider[6];

                var size = Physics.OverlapSphereNonAlloc(snapPoint.pointTransform.position, 10f, cols,
                                                         LayerMask.GetMask("Road"));

                if (cols.Count(t => t != null && t.transform.parent != snapPoint.pointTransform.parent) > 2)
                {
                    snapPoint.isOccupied = true;
                    continue;
                }

//                var width = Random.Range(3, 6);
//                var height = Random.Range(3, 6);
//                float blockSize = 230f;
//                if (Random.value * 1000f <= roadWebPreset.cityChance &&
//                    Vector3.Distance(current.gameObject.transform.position, lastCityPoint).Log() > 1000f)
//                {
//                    Transform cityParent = null;
//                    RoadPiece parent = null;
//
//                    GameObject newBlock;
//
//                    for (int yIndex = 0; yIndex < height; yIndex++)
//                    {
//                        for (int xIndex = 0; xIndex < width; xIndex++)
//                        {
//                            if (xIndex == 0 && yIndex == 0)
//                            {
//                                newBlock = Instantiate(roadWebPreset.mainCityBlock,
//                                    new Vector3(xIndex * blockSize, 0f, yIndex * blockSize), Quaternion.identity);
//                                cityParent = newBlock.transform;
//                                parent = new RoadPiece(newBlock);
//                                _roadPieces.Add(parent);
//                                continue;
//                            }
//                            else if (xIndex < width - 1 && yIndex < height - 1)
//                            {
//                                newBlock = Instantiate(roadWebPreset.mainCityBlock,
//                                    new Vector3(xIndex * blockSize, 0f, yIndex * blockSize), Quaternion.identity,
//                                    cityParent);
//                                var newRoadPiece = new RoadPiece(newBlock);
//                                newRoadPiece.snapPoints.ForEach(t => t.isOccupied = true);
//                                _roadPieces.Add(newRoadPiece);
//                                continue;
//                            }
//                            else if (xIndex == width - 1 && yIndex < height - 1)
//                            {
//                                newBlock = Instantiate(roadWebPreset.endRowCityBlock,
//                                    new Vector3(xIndex * blockSize, 0f, yIndex * blockSize), Quaternion.identity,
//                                    cityParent);
//                            }
//                            else if (yIndex == height - 1 && xIndex < width - 1)
//                            {
//                                newBlock = Instantiate(roadWebPreset.lastRowCityBlock,
//                                    new Vector3(xIndex * blockSize, 0f, yIndex * blockSize), Quaternion.identity,
//                                    cityParent);
//                            }
//                            else
//                            {
//                                newBlock = Instantiate(roadWebPreset.lastCityBlock,
//                                    new Vector3(xIndex * blockSize, 0f, yIndex * blockSize), Quaternion.identity,
//                                    cityParent);
//                            }
//
//                            _roadPieces.Add(new RoadPiece(newBlock));
//                        }
//                    }
//
//                    current.SnapToRoadPiece(parent);
//                    cityParent.SetParent(_roadParent);
//
//                    lastCityPoint = cityParent.position;
//                }
//                else
                {
                    var randomObject     = _roadPrefabRandomList.GetRandom();
                    var newSnapRoadPiece = new RoadPiece(randomObject);
                    current.SnapToRoadPiece(newSnapRoadPiece);
                    newSnapRoadPiece.gameObject.transform.SetParent(_roadParent);

                    var closestGridPoint = VertexAccess.ClosestMapPoint(newSnapRoadPiece.gameObject.transform.position);

                    for (var i = -2; i <= 2; i++)
                    {
                        for (var j = -2; j < 2; j++)
                        {
                            VertexAccess.SetWorldVertexHeightAtPoint(
                                closestGridPoint.x + j * WorldGenerator.meshGeneratorPreset.mapSettings.gridSize,
                                closestGridPoint.z + i * WorldGenerator.meshGeneratorPreset.mapSettings.gridSize, 0f);
                        }
                    }

                    _roadPieces.Add(newSnapRoadPiece);
                }

                count++;
            }

            return count;
        }
    }
}