using UnityEngine;

// This class simulates a single car's wheel with tire, brake and simple
// suspension (basically just a single, independant spring and damper).
public class Wheel : MonoBehaviour
{
    // Wheel Specifications

    // Wheel radius in meters
    public float radius = 0.34f;

    // Wheel suspension travel in meters
    public float suspensionTravel = 0.2f;

    // Damper strength in kg/s
    public float damping = 5000;

    // Wheel angular inertia in kg * m^2
    public float inertia = 2.2f;

    // Coeefficient of grip - this is simly multiplied to the resulting forces, 
    // so it is not quite realitic, but an easy way to quickly change handling characteritics
    public float grip = 1.0f;

    // Maximal braking torque (in Nm)
    public float brakeFrictionTorque = 4000;

    // Maximal handbrake torque (in Nm)
    public float handbrakeFrictionTorque;

    // Base friction torque (in Nm)
    public float frictionTorque = 10;

    // Maximal steering angle (in degrees)
    public float maxSteeringAngle = 28f;

    // Graphical wheel representation (to be rotated accordingly)
    public GameObject model;

    // Fraction of the car's mass carried by this wheel
    public float massFraction = 0.25f;

    // Pacejka coefficients
    public float[] a =
        { 1.0f, -60f, 1688f, 4140f, 6.026f, 0f, -0.3589f, 1f, 0f, -6.111f / 1000f, -3.244f / 100f, 0f, 0f, 0f, 0f };

    public float[] b = { 1.0f, -60f, 1588f, 0f, 229f, 0f, 0f, 0f, -10f, 0f, 0f };

    // inputs
    // engine torque applied to this wheel
    public float driveTorque;

    // engine braking and other drivetrain friction torques applied to this wheel
    public float driveFrictionTorque;

    // brake input
    public float brake;

    // handbrake input
    public float handbrake;

    // steering input
    public float steering;

    // drivetrain inertia as currently connected to this wheel
    public float drivetrainInertia;

    // suspension force externally applied (by anti-roll bars)
    public float suspensionForceInput;

    // output
    public float angularVelocity;
    public float slipRatio;
    public float slipVelo;
    public float compression;

    public bool onGround;
    //int lastSkid = -1;

    // cached values
    private Rigidbody body;

    private float counterForce;

    // state
    private float fullCompressionSpringForce;

    //Skidmarks skid;
    private string     groundName = "Ground";
    private Vector3    groundNormal;
    private Quaternion inverseLocalRotation = Quaternion.identity;
    private Quaternion localRotation        = Quaternion.identity;
    private Vector3    localVelo;
    private float      maxAngle;
    private float      maxSlip;
    private float      normalForce;
    private float      oldAngle;
    private Vector3    roadForce;
    private float      rotation;
    private float      slipAngle;
    private Vector3    suspensionForce;
    private Vector3    up, right, forward;
    private Vector3    wheelVelo;

    private void Start()
    {
        var trs = transform;
        while (trs != null && trs.GetComponent<Rigidbody>() == null)
        {
            trs = trs.parent;
        }

        if (trs != null)
        {
            body = trs.GetComponent<Rigidbody>();
        }

        InitSlipMaxima();
        //skid = FindObjectOfType(typeof(Skidmarks)) as Skidmarks;
        fullCompressionSpringForce = body.mass * massFraction * 2.0f * -Physics.gravity.y;
    }

    private void FixedUpdate()
    {
        var pos = transform.position;
        up = transform.up;
        RaycastHit hit;
        onGround = Physics.Raycast(pos, -up, out hit, suspensionTravel + radius);
        if (hit.collider != null)
        {
            groundName = hit.collider.gameObject.tag;
            Debug.DrawLine(model.transform.position, hit.point, Color.magenta);
        }

        if (onGround && hit.collider.isTrigger)
        {
            onGround = false;
            var dist = suspensionTravel + radius;
            var hits = Physics.RaycastAll(pos, -up, suspensionTravel + radius);
            foreach (var test in hits)
            {
                if (!test.collider.isTrigger && test.distance <= dist)
                {
                    hit      = test;
                    onGround = true;
                    dist     = test.distance;
                }
            }
        }

        if (onGround)
        {
            groundNormal    = transform.InverseTransformDirection(inverseLocalRotation * hit.normal);
            compression     = 1.0f - (hit.distance - radius) / suspensionTravel;
            wheelVelo       = body.GetPointVelocity(pos);
            localVelo       = transform.InverseTransformDirection(inverseLocalRotation * wheelVelo);
            suspensionForce = SuspensionForce();

            var speed = (int)(GetComponentInParent<Rigidbody>().velocity.magnitude * 3.6f);

            // if car is stopped , do the following to avoid slipping (currently as a whole instead of just sideways , #tbf)
            if (speed == 0)
            {
                suspensionForce = new Vector3(0f, suspensionForce.y, 0f);
            }

            roadForce = RoadForce();


            body.AddForceAtPosition(suspensionForce + roadForce, pos);
        }
        else
        {
            compression     = 0.0f;
            suspensionForce = Vector3.zero;
            roadForce       = Vector3.zero;
            var totalInertia      = inertia + drivetrainInertia;
            var driveAngularDelta = driveTorque * Time.deltaTime / totalInertia;
            var totalFrictionTorque = brakeFrictionTorque * brake + handbrakeFrictionTorque * handbrake +
                frictionTorque                                    + driveFrictionTorque;
            var frictionAngularDelta = totalFrictionTorque * Time.deltaTime / totalInertia;
            angularVelocity += driveAngularDelta;
            if (Mathf.Abs(angularVelocity) > frictionAngularDelta)
            {
                angularVelocity -= frictionAngularDelta * Mathf.Sign(angularVelocity);
            }
            else
            {
                angularVelocity = 0;
            }


            slipRatio = 0;
            slipVelo  = 0;
        }

        /*
        if (skid != null && Mathf.Abs(slipRatio) > 0.2) {
            if (groundName == "Road")
                lastSkid = skid.AddSkidMark(hit.point, hit.normal, Mathf.Abs(slipRatio) - 0.2f, lastSkid);
        } else {
            lastSkid = -1;
        }
        */

        compression     =  Mathf.Clamp01(compression);
        rotation        += angularVelocity * Time.deltaTime;
        angularVelocity =  angularVelocity < -45f ? -45f : angularVelocity;

        if (model != null)
        {
            model.transform.localPosition = Vector3.up * (compression - 1.0f) * suspensionTravel;
            model.transform.localRotation = Quaternion.Euler(Mathf.Rad2Deg * rotation, maxSteeringAngle * steering, 0);
        }
    }

    private float CalcLongitudinalForce(float Fz, float slip)
    {
        Fz   *= 0.001f; //convert to kN
        slip *= 100f;   //covert to %
        var uP = b[1] * Fz + b[2];
        var D  = uP                                         * Fz;
        var B  = (b[3] * Fz + b[4]) * Mathf.Exp(-b[5] * Fz) / (b[0] * uP);
        var S  = slip + b[9]        * Fz      + b[10];
        var E  = b[6]               * Fz * Fz + b[7] * Fz + b[8];
        var Fx = D * Mathf.Sin(b[0] * Mathf.Atan(S * B + E * (Mathf.Atan(S * B) - S * B)));
        return Fx;
    }

    private float CalcLateralForce(float Fz, float slipAngle)
    {
        Fz        *= 0.001f;                //convert to kN
        slipAngle *= 360f / (2 * Mathf.PI); //convert angle to deg
        var uP = a[1] * Fz + a[2];
        var D  = uP                                                      * Fz;
        var B  = a[3]             * Mathf.Sin(2 * Mathf.Atan(Fz / a[4])) / (a[0] * uP * Fz);
        var S  = slipAngle + a[9] * Fz                                                                    + a[10];
        var E  = a[6]             * Fz                                                                    + a[7];
        var Sv = a[12]            * Fz                                                                    + a[13];
        var Fy = D                * Mathf.Sin(a[0] * Mathf.Atan(S * B + E * (Mathf.Atan(S * B) - S * B))) + Sv;
        return Fy;
    }

    private float CalcLongitudinalForceUnit(float Fz, float slip)
    {
        return CalcLongitudinalForce(Fz, slip * maxSlip);
    }

    private float CalcLateralForceUnit(float Fz, float slipAngle)
    {
        return CalcLongitudinalForce(Fz, slipAngle * maxAngle);
    }

    private Vector3 CombinedForce(float Fz, float slip, float slipAngle)
    {
        var unitSlip  = slip      / maxSlip;
        var unitAngle = slipAngle / maxAngle;
        var p         = Mathf.Sqrt(unitSlip * unitSlip + unitAngle * unitAngle);
        if (p > Mathf.Epsilon)
        {
            if (slip < -0.8f)
            {
                return -localVelo.normalized * (Mathf.Abs(unitAngle / p * CalcLateralForceUnit(Fz, p)) +
                    Mathf.Abs(unitSlip                              / p * CalcLongitudinalForceUnit(Fz, p)));
            }

            var forward = new Vector3(0, -groundNormal.z, groundNormal.y);
            return Vector3.right * unitAngle / p * CalcLateralForceUnit(Fz, p) +
                forward          * unitSlip  / p * CalcLongitudinalForceUnit(Fz, p);
        }

        return Vector3.zero;
    }

    private void InitSlipMaxima()
    {
        const float stepSize        = 0.001f;
        const float testNormalForce = 4000f;
        float       force           = 0;
        for (var slip = stepSize;; slip += stepSize)
        {
            var newForce = CalcLongitudinalForce(testNormalForce, slip);
            if (force < newForce)
            {
                force = newForce;
            }
            else
            {
                maxSlip = slip - stepSize;
                break;
            }
        }

        force = 0;
        for (var slipAngle = stepSize;; slipAngle += stepSize)
        {
            var newForce = CalcLateralForce(testNormalForce, slipAngle);
            if (force < newForce)
            {
                force = newForce;
            }
            else
            {
                maxAngle = slipAngle - stepSize;
                break;
            }
        }
    }

    private Vector3 SuspensionForce()
    {
        var springForce = compression * fullCompressionSpringForce;
        normalForce = springForce;

        var damperForce = Vector3.Dot(localVelo, groundNormal) * damping;

        return (springForce - damperForce + suspensionForceInput) * up;
    }

    private float SlipRatio()
    {
        const float fullSlipVelo = 4.0f;

        var wheelRoadVelo = Vector3.Dot(wheelVelo, forward);
        if (wheelRoadVelo == 0)
        {
            return 0;
        }

        var absRoadVelo = Mathf.Abs(wheelRoadVelo);
        var damping     = Mathf.Clamp01(absRoadVelo / fullSlipVelo);

        var wheelTireVelo = angularVelocity * radius;

        return (wheelTireVelo - wheelRoadVelo) / absRoadVelo * damping;
    }

    private float SlipAngle()
    {
        const float fullAngleVelo = 2.0f;

        var wheelMotionDirection = localVelo;
        wheelMotionDirection.y = 0;

        if (wheelMotionDirection.sqrMagnitude < Mathf.Epsilon)
        {
            return 0;
        }

        var sinSlipAngle = wheelMotionDirection.normalized.x;
        Mathf.Clamp(sinSlipAngle, -1, 1); // To avoid precision errors.

        var damping = Mathf.Clamp01(localVelo.magnitude / fullAngleVelo);

        return -Mathf.Asin(sinSlipAngle) * damping * damping;
    }

    private Vector3 RoadForce()
    {
        var slipRes = (int)((100.0f - Mathf.Abs(angularVelocity)) / 10.0f);
        if (slipRes < 1)
        {
            slipRes = 1;
        }

        var invSlipRes = 1.0f / slipRes;

        var totalInertia      = inertia + drivetrainInertia;
        var driveAngularDelta = driveTorque * Time.deltaTime * invSlipRes / totalInertia;
        var totalFrictionTorque = brakeFrictionTorque * brake + handbrakeFrictionTorque * handbrake + frictionTorque +
            driveFrictionTorque;
        var frictionAngularDelta = totalFrictionTorque * Time.deltaTime * invSlipRes / totalInertia;

        var totalForce = Vector3.zero;
        var newAngle   = maxSteeringAngle * steering;
        for (var i = 0; i < slipRes; i++)
        {
            var f = i * 1.0f / slipRes;
            localRotation        = Quaternion.Euler(0, oldAngle + (newAngle - oldAngle) * f, 0);
            inverseLocalRotation = Quaternion.Inverse(localRotation);
            forward              = transform.TransformDirection(localRotation * Vector3.forward);
            right                = transform.TransformDirection(localRotation * Vector3.right);

            slipRatio = SlipRatio();
            slipAngle = SlipAngle();

            var force      = invSlipRes * grip * CombinedForce(normalForce, slipRatio, slipAngle);
            var worldForce = transform.TransformDirection(localRotation * force);
            angularVelocity -= force.z * radius * Time.deltaTime / totalInertia;
            angularVelocity += driveAngularDelta;

            if (Mathf.Abs(angularVelocity) > frictionAngularDelta)
            {
                angularVelocity -= frictionAngularDelta * Mathf.Sign(angularVelocity);
            }
            else
            {
                angularVelocity = 0;
            }


            wheelVelo  += worldForce * (1 / body.mass) * Time.deltaTime * invSlipRes;
            totalForce += worldForce;
        }

        var longitunalSlipVelo = Mathf.Abs(angularVelocity * radius - Vector3.Dot(wheelVelo, forward));
        var lateralSlipVelo    = Vector3.Dot(wheelVelo, right);
        slipVelo = Mathf.Sqrt(longitunalSlipVelo * longitunalSlipVelo + lateralSlipVelo * lateralSlipVelo);
        oldAngle = newAngle;
        return totalForce;
    }
}