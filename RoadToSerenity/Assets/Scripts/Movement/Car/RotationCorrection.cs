﻿using System.Collections;
using UnityEngine;

public class RotationCorrection : MonoBehaviour
{
    public float flipRotation = 180;

    public float     minAirRotation        = 10f, maxAirRotation = 25f;
    public float     rotationIncrementStep = 0.5f;
    public float     airTimeThreshold      = 1f;
    public Transform alignRotation;

    private TriggerLoading _triggerLoading;
    private CarController  _carController;
    private Rigidbody      _rigidbody;
    private float          _currentRotationSpeed;
    private float          _airTimer;
    private bool           _isLanded, _isCounting;

    private void Start()
    {
        _rigidbody      = GetComponent<Rigidbody>();
        _triggerLoading = FindObjectOfType<TriggerLoading>();
        _carController  = FindObjectOfType<CarController>();
    }

    private void FlipCar()
    {
        transform.rotation =
            Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * flipRotation);
    }

    private void AirCorrection()
    {
        // Applies just enough rotational force to straighten car in air
        var rotation      = transform.rotation;
        var rotationForce = Time.fixedDeltaTime * maxAirRotation;
//        if (_triggerLoading.raycastHit.distance < 15f)
        {
            rotationForce *= 1 / Mathf.Max(0.001f,
                                           (_triggerLoading.raycastHit.distance - 3f) *
                                           (_triggerLoading.raycastHit.distance - 3f));
        }

        rotation = Quaternion.RotateTowards(rotation, Quaternion.Euler(0f, alignRotation.rotation.eulerAngles.y, 0f),
                                            rotationForce);

//        if (_currentRotationSpeed < maxAirRotation)
//            _currentRotationSpeed += rotationIncrementStep;

        _rigidbody.angularVelocity -= _rigidbody.angularVelocity / 40f;
//        _rigidbody.AddTorque(-_rigidbody.angularVelocity);
        _rigidbody.MoveRotation(rotation);
    }

#if false
    private void OnGUI()
    {
        GUI.TextArea(new Rect(10f, 100f, 100f, 100f),
            $"Velocity: {_rigidbody.velocity.magnitude}\nAngular velocity: {_rigidbody.angularVelocity.magnitude}");
    }
#endif

//    private void OnTriggerStay(Collider other1)
//    {
//        _isLanded = true;
//    }
//
//    private void OnTriggerEnter(Collider other1)
//    {
//        _isLanded = true;
//        StartCoroutine(TrackAirTime(_isLanded));
//    }
//
//    private void OnTriggerExit(Collider other1)
//    {
//        _isLanded = false;
//        StartCoroutine(TrackAirTime(_isLanded));
//    }

    private void Update()
    {
        if (!CarController.grounded)
        {
            _isLanded = false;
            StartCoroutine(TrackAirTime(_isLanded));
        }
        else
        {
            _isLanded = true;
            StartCoroutine(TrackAirTime(_isLanded));
        }
    }

    private IEnumerator TrackAirTime(bool isOnGround)
    {
        if (!isOnGround)
        {
            if (_isCounting)
            {
                yield break;
            }

            _airTimer   = 0f;
            _isCounting = true;
            while (_airTimer < airTimeThreshold)
            {
                if (_isLanded)
                {
                    Stop();
                    yield break;
                }

                _airTimer += Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }

            _isCounting = false;

            while (!_isLanded) //&& Quaternion.Angle(transform.rotation, alignRotation.rotation) > 1f
            {
                AirCorrection();
                yield return new WaitForEndOfFrame();
            }

            Stop();
        }
        else
        {
            Stop();
        }

        void Stop()
        {
            _airTimer             = 0f;
            _isCounting           = false;
            _currentRotationSpeed = minAirRotation;
            StopCoroutine(TrackAirTime(false));
        }
    }
}