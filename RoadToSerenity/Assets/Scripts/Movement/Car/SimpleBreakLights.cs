﻿using Rewired;
using UnityEngine;

public class SimpleBreakLights : MonoBehaviour
{
    public  Material breakLight;
    private Player   controls;
    private Material inActive;

    private Renderer renderer;

    // Start is called before the first frame update
    private void Start()
    {
        controls = ReInput.players.GetPlayer(0);
        renderer = GetComponent<Renderer>();
        inActive = renderer.material;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (controls.GetButton("Reverse") || controls.GetButton("Handbrake"))
        {
            if (CarController.isFlying) { }
            else
            {
                renderer.material = breakLight;
            }
        }
        else
        {
            renderer.material = inActive;
        }
    }
}