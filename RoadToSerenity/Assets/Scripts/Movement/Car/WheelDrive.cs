﻿using System;
using Rewired;
using UnityEngine;

[Serializable]
public enum DriveType
{
    RearWheelDrive,
    FrontWheelDrive,
    AllWheelDrive
}

public class WheelDrive : MonoBehaviour
{
    [Tooltip("Maximum steering angle of the wheels")]
    public float maxAngle = 30f;

    [Tooltip("Maximum torque applied to the driving wheels")]
    public float maxTorque = 300f;

    [Tooltip("Maximum brake torque applied to the driving wheels")]
    public float brakeTorque = 30000f;

    [Tooltip("If you need the visual wheels to be attached automatically, drag the wheel shape here.")]
    public GameObject wheelShape;

    [Tooltip("The vehicle's speed when the physics engine can use different amount of sub-steps (in m/s).")]
    public float criticalSpeed = 5f;

    [Tooltip("Simulation sub-steps when the speed is above critical.")]
    public int stepsBelow = 5;

    [Tooltip("Simulation sub-steps when the speed is below critical.")]
    public int stepsAbove = 1;

    [Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
    public DriveType driveType;
    // Horizontal and vertical axis values

    public float reWireAd, reWireQe;

    // Forward, reverse and menu bools
    public bool reWireW, reWireS, reWireZ;

    public float torque;

    public Material breakLights;

    // The assigned controller of the player, NOT TO BE CONFUSED WITH THE GAMEOBJECT "player"
    private Player _playerController;

    private WheelCollider[] m_Wheels;

    // Rewired controlled values v1
    private readonly int playerControllerId = 0; // Make public if we ever want to add more players

    private void Awake()
    {
        _playerController = ReInput.players.GetPlayer(playerControllerId);
    }


    // Find all the WheelColliders down in the hierarchy.
    private void Start()
    {
        m_Wheels = GetComponentsInChildren<WheelCollider>();

        for (var i = 0; i < m_Wheels.Length; ++i)
        {
            var wheel = m_Wheels[i];

            // Create wheel shapes only when needed.
            if (wheelShape != null)
            {
                var ws = Instantiate(wheelShape, wheel.transform);
            }
        }
    }

    // This is a really simple approach to updating wheels.
    // We simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero.
    // This helps us to figure our which wheels are front ones and which are rear.
    private void Update()
    {
        if (breakLights != null)
        {
            breakLights.SetVector("_EmissionColor", new Vector3(0.1f, 0.1f, 0.1f));
        }

        m_Wheels[0].ConfigureVehicleSubsteps(criticalSpeed, stepsBelow, stepsAbove);

        // Rewired controller
        reWireQe = _playerController.GetAxis("Vertical"); // Q and E tile for glider

        var angle = maxAngle * _playerController.GetAxis("Horizontal"); // A and D for steering
        reWireAd = angle;


        // Controls forward and backwards movement
        if (_playerController.GetButton("Forward"))
        {
            reWireW = true;
            if (breakLights != null)
            {
                breakLights.SetVector("_EmissionColor", new Vector3(0.1f, 0.1f, 0.1f));
            }
        }
        else
        {
            reWireW = false;
        }

        if (_playerController.GetButton("Reverse"))
        {
            reWireS = true;
            if (breakLights != null)
            {
                breakLights.SetVector("_EmissionColor", new Vector3(4, 4, 4));
            }
        }
        else
        {
            reWireS = false;
        }

        if (!reWireW || !reWireS)
        {
            torque = 0;
            if (reWireW) // W for forward
            {
                torque = maxTorque;
            }

            if (reWireS) // S for Reverse
            {
                torque = -maxTorque;
            }
        }

        var handBrake = _playerController.GetButton("Handbrake") ? brakeTorque : 0;
        if (handBrake > 0)
        {
            if (breakLights != null)
            {
                breakLights.SetVector("_EmissionColor", new Vector3(4, 4, 4));
            }
        }

        foreach (var wheel in m_Wheels)
        {
            // A simple car where front wheels steer while rear ones drive.
            if (wheel.transform.localPosition.z > 0)
            {
                wheel.steerAngle = angle;
            }

            if (wheel.transform.localPosition.z < 0)
            {
                wheel.brakeTorque = handBrake;
            }

            if (wheel.transform.localPosition.z < 0 && driveType != DriveType.FrontWheelDrive)
            {
                wheel.motorTorque = torque;
            }

            if (wheel.transform.localPosition.z >= 0 && driveType != DriveType.RearWheelDrive)
            {
                wheel.motorTorque = torque;
            }

            // Update visual wheels if any.
            if (wheelShape)
            {
                Quaternion q;
                Vector3    p;
                wheel.GetWorldPose(out p, out q);

                // Assume that the only child of the wheelcollider is the wheel shape.
                var shapeTransform = wheel.transform.GetChild(0);

                if (wheel.name == "RR" || wheel.name == "FR")
                {
                    shapeTransform.rotation = q * Quaternion.Euler(0, 180, 0);
                    shapeTransform.position = p;
                }
                else
                {
                    shapeTransform.position = p;
                    shapeTransform.rotation = q;
                }
            }
        }
    }
}