﻿using UnityEngine;

public class RigidbodyOnCOLL : MonoBehaviour
{
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        rb.useGravity = true;
        Destroy(gameObject, 5f);
    }
}