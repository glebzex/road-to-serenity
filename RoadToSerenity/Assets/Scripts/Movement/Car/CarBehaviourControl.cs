﻿using System.Collections;
using UnityEngine;

// This class simulates an anti-roll bar.
// Anti roll bars transfer suspension compressions forces from one wheel to another.
// This is used to minimize body roll in corners, and improve grip by balancing the wheel loads.
// Typical modern cars have one anti-roll bar per axle.
public class CarBehaviourControl : MonoBehaviour
{
    // Friction coefficients along the X, Y and Z-Axes of the car.
    // A higher Z-Friction coefficient will make a car slower.
    public Vector3 coefficients = new(3.0f, 4.0f, 0.5f);

    public bool isgrounded = true;

    //Front Left and Right side
    public WheelCollider WheelFL;
    public WheelCollider WheelFR;

    //Rear Left and Right side
    public WheelCollider WheelRL;
    public WheelCollider WheelRR;


    public bool wheels;

    //Tells ghow much resistance the car should have against rolling.
    //    If it is turned down then the car rolls easier again.
    public float AntiRoll = 5000.0f;

    private Rigidbody carRigidBody;


    private void Start()
    {
        carRigidBody = GetComponent<Rigidbody>();

        //Changes the location of the center of mass on the car
        GetComponent<Rigidbody>().centerOfMass = new Vector3(0, -0.1f, 0);
    }

    private void Update()
    {
        // Velocity in local space.
        var localVelo = transform.InverseTransformDirection(GetComponent<Rigidbody>().velocity);

        // Strip signs.
        var absLocalVelo = new Vector3(Mathf.Abs(localVelo.x), Mathf.Abs(localVelo.y), Mathf.Abs(localVelo.z));

        // Calculate and apply aerodynamic force.
        var airResistance = Vector3.Scale(Vector3.Scale(localVelo, absLocalVelo), -2 * coefficients);
        GetComponent<Rigidbody>().AddForce(transform.TransformDirection(airResistance));
    }

    private void FixedUpdate()
    {
        var hit     = new WheelHit();
        var travelL = 1.0f;
        var travelR = 1.0f;

        var groundedL = WheelFL.GetGroundHit(out hit);

        if (groundedL)
        {
            travelL = (-WheelFL.transform.InverseTransformPoint(hit.point).y
              - WheelFL.radius) / WheelFL.suspensionDistance;
        }

        var groundedR = WheelFR.GetGroundHit(out hit);

        if (groundedR)
        {
            travelR = (-WheelFR.transform.InverseTransformPoint(hit.point).y
              - WheelFR.radius) / WheelFR.suspensionDistance;
        }

        var antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL)
        {
            carRigidBody.AddForceAtPosition(WheelFL.transform.up * -antiRollForce,
                                            WheelFL.transform.position);
        }

        if (groundedR)
        {
            carRigidBody.AddForceAtPosition(WheelFR.transform.up * antiRollForce,
                                            WheelFR.transform.position);
        }

        if (groundedL)
        {
            carRigidBody.AddForceAtPosition(WheelRL.transform.up * -antiRollForce,
                                            WheelRL.transform.position);
        }

        if (groundedR)
        {
            carRigidBody.AddForceAtPosition(WheelRR.transform.up * antiRollForce,
                                            WheelRR.transform.position);
        }

        //If the buttong "G" is pressed it starts a timer for turning the car back on it's wheels again.
        if (Input.GetKeyUp("g"))
        {
            StartCoroutine(Timer());
        }
    }

    //Checks if the car is hitting the ground
    private void OnTriggerEnter(Collider other1)
    {
        isgrounded = true;
    }

    //Checks if the care is jumping off the ground, if so, it unticks the isgrounded bool to false.
    private void OnTriggerExit(Collider other1)
    {
        isgrounded = false;
    }

    //Chekcs if the car if on the ground, and it makes it stay ticked
    private void OnTriggerStay(Collider other1)
    {
        isgrounded = true;
    }

    private IEnumerator Timer()
    {
        wheels = GameObject.Find("Car").GetComponent<WheelDrive>();
        wheels = false;
        Debug.Log("Car Should be disabled?");

        yield return new WaitForSeconds(5);

        wheels = true;
        Debug.Log("Something is working?");
    }
}