﻿using UnityEngine;

[ExecuteInEditMode]
public class CustomCenterOfMass : MonoBehaviour
{
    public  float     centerOfMassOffset;
    private Rigidbody _rigidbody;

//#if UNITY_EDITOR
//    private void OnDrawGizmos()
//    {
//        Gizmos.DrawSphere(_rigidbody.worldCenterOfMass, .2f);
//    }
//
//    // Update is called once per frame
//    private void Update()
//    {
//        ChangeCenterOfMass();
//    }
//#endif

    // Start is called before the first frame update
    private void Start()
    {
        ChangeCenterOfMass();
    }


    private void ChangeCenterOfMass()
    {
        _rigidbody = GetComponent<Rigidbody>();
        var currentCenter = _rigidbody.centerOfMass;
        _rigidbody.centerOfMass = new Vector3(currentCenter.x, currentCenter.y - centerOfMassOffset, currentCenter.z);
    }
}