﻿using UnityEngine;

public class AntriRollBar : MonoBehaviour
{
    private Rigidbody m_Rigidbody;

    private void Start()
    {
        m_Rigidbody                            = GetComponent<Rigidbody>();
        GetComponent<Rigidbody>().centerOfMass = new Vector3(0, -1, 0);
    }
}