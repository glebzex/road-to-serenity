﻿using UnityEngine;

public class TransformLookAt : MonoBehaviour
{
    public Transform target, direct;

    public WheelCS[] wheels;

    public bool allGrounded;

    public  float   timer, lerpTimer, slerpTimer;
    private bool    once;
    private Vector3 savePos;

    private Quaternion startRot, lookRot;

    // Start is called before the first frame update
    private void Start()
    {
        startRot = Quaternion.Euler(0, 0, 0);
    }

    // Update is called once per frame
    private void Update()
    {
        allGrounded = false;
        for (var i = 0; i < wheels.Length; i++)
        {
            if (wheels[i].onGround)
            {
                allGrounded = true;
            }
        }

        if (!allGrounded)
        {
            timer += Time.deltaTime;
            if (timer > 0.15f)
            {
                if (!once)
                {
                    savePos = transform.position;
                    once    = true;
                }

                direct.LookAt(target, Vector3.up);
                slerpTimer += Time.deltaTime;
                //transform.LookAt(target.position, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, direct.rotation, slerpTimer);
            }
            else
            {
                slerpTimer = 0;
            }

            lookRot   = transform.localRotation;
            lerpTimer = 0;
        }
        else
        {
            timer                   =  0;
            lerpTimer               += Time.deltaTime * 3;
            transform.localRotation =  Quaternion.Lerp(lookRot, startRot, lerpTimer);
            once                    =  false;
        }
    }
}