using System;
using UnityEngine;

public class DriftCamera : MonoBehaviour
{
    public float           smoothing = 6f;
    public Transform       lookAtTarget;
    public Transform       positionTarget;
    public Transform       sideView;
    public AdvancedOptions advancedOptions;

    public  bool                _isgrounded;
    private CarBehaviourControl _carBehaviourControl;

    private bool _showingSideView;

    public void Start()
    {
        _carBehaviourControl = FindObjectOfType<CarBehaviourControl>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(advancedOptions.switchViewKey))
        {
            _showingSideView = !_showingSideView;
        }


        IsGrounded();
    }

    private void FixedUpdate()
    {
        UpdateCamera();
    }

    private void UpdateCamera()
    {
        var transformCached = transform;
        if (_showingSideView)
        {
            transformCached.position = sideView.position;
            transformCached.rotation = sideView.rotation;
        }
        else
        {
            gameObject.transform.position =
                Vector3.Lerp(transformCached.position, positionTarget.position, Time.deltaTime * smoothing);
            transformCached.LookAt(lookAtTarget);
        }
    }

    public void AlignWithAnchorPoint()
    {
        InstantlyChangePosition(positionTarget.position);
    }

    public void InstantlyChangePosition(Vector3 newPosition)
    {
        transform.position = newPosition;
        transform.LookAt(lookAtTarget);
    }

    private void IsGrounded()
    {
        _isgrounded = _carBehaviourControl.isgrounded;

        if (_isgrounded)
        {
            //if (advancedOptions.updateCameraInLateUpdate) ;
        }

        if (_isgrounded == false) { }
    }

    [Serializable]
    public class AdvancedOptions
    {
        public bool    updateCameraInUpdate;
        public bool    updateCameraInFixedUpdate = true;
        public bool    updateCameraInLateUpdate;
        public KeyCode switchViewKey = KeyCode.Space;
    }
}