﻿using UnityEngine;

public class CarCamera_01 : MonoBehaviour
{
    public  float   distance = 3.0f;
    public  float   height   = 3.0f;
    public  float   damping  = 5.0f;
    public  bool    smoothRotation;
    public  bool    followBehind = true;
    public  float   rotationDamping;
    private Vector3 CameraPos;

    //Transform of the car that it is supposed to follow
    private Transform Car;

    // Start is called before the first frame update
    private void Start()
    {
        //Finding the position of the car
        Car = GameObject.FindWithTag("Player").transform;

        CameraPos = transform.position - Car.transform.position;
    }

    private void Update()
    {
        Vector3 wantedPosition;
        if (followBehind)
        {
            wantedPosition = Car.TransformPoint(0, height, -distance);
        }
        else
        {
            wantedPosition = Car.TransformPoint(0, height, distance);
        }

        transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * damping);

        if (smoothRotation)
        {
            var wantedRotation = Quaternion.LookRotation(Car.position - transform.position, Car.up);
            transform.rotation = Quaternion.Slerp(transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
        }
        else
        {
            transform.LookAt(Car, Car.up);
        }
    }
}