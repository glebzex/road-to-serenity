﻿using System;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Triggers/DetectionTrigger")]
public class DetectionTrigger : MonoBehaviour
{
    public bool IsTripped
    {
        get
        {
            if (Colliders.Count == 0)
            {
                return false;
            }

            var isTripped = false;
            foreach (var t in Colliders.Values)
            {
                if (!Ignores.Contains(t))
                {
                    isTripped = true;
                }
            }

            return isTripped;
        }
    }

    #region Private Methods

    private Collider GetCollider()
    {
        Collider myCollider = null;
        switch (colliderType)
        {
            case ColliderEnumType.Box:
                myCollider = GetComponent(typeof(BoxCollider)) as BoxCollider;
                if (myCollider == null)
                {
                    myCollider = GameObject.AddComponent(typeof(BoxCollider)) as BoxCollider;
                }

                break;
            case ColliderEnumType.Capsule:
                myCollider = GetComponent(typeof(CapsuleCollider)) as CapsuleCollider;
                if (myCollider == null)
                {
                    myCollider = GameObject.AddComponent(typeof(CapsuleCollider)) as CapsuleCollider;
                }

                break;
            case ColliderEnumType.Sphere:
                myCollider = GetComponent(typeof(SphereCollider)) as SphereCollider;
                if (myCollider == null)
                {
                    myCollider = GameObject.AddComponent(typeof(SphereCollider)) as SphereCollider;
                }

                break;
            case ColliderEnumType.Wheel:
                myCollider = GetComponent(typeof(WheelCollider)) as WheelCollider;
                if (myCollider == null)
                {
                    myCollider = GameObject.AddComponent(typeof(WheelCollider)) as WheelCollider;
                }

                break;
            case ColliderEnumType.Mesh:
                myCollider = GetComponent(typeof(MeshCollider)) as MeshCollider;
                if (myCollider == null)
                {
                    myCollider = GameObject.AddComponent(typeof(MeshCollider)) as MeshCollider;
                }

                break;
        }

        if (myCollider == null)
        {
            throw new Exception("Trigger Item Has No Collider");
        }

        return myCollider;
    }

    #endregion

    #region Properties

    #region Private Properties

    private GameObject ourGameObject;

    private GameObject GameObject
    {
        get
        {
            if (ourGameObject == null)
            {
                ourGameObject = gameObject;
            }

            return ourGameObject;
        }
    }

    protected Collider ourCollider;

    protected Collider Collider
    {
        get
        {
            if (ourCollider == null)
            {
                ourCollider           = GetCollider();
                ourCollider.isTrigger = true;
            }

            return ourCollider;
        }
    }

    #endregion

    #region Public Properties

    public enum ColliderEnumType
    {
        Box,
        Capsule,
        Sphere,
        Wheel,
        Mesh
    }

    [SerializeField]
    private ColliderEnumType colliderType = ColliderEnumType.Sphere;

    public ColliderEnumType ColliderType
    {
        get => colliderType;
        set => colliderType = value;
    }

    public Dictionary<int, Transform> Colliders { get; set; } = new();

    public List<Transform> Ignores { get; set; } = new();

    public List<Type> IgnoreTypes { get; set; } = new();

    #endregion

    #endregion

    #region Unity Methods

    public void Awake()
    {
        if (Collider)
        {
            ;
        }
    }

    private void OnTriggerEnter(Collider argCollider)
    {
        Debug.Log(argCollider.transform.GetInstanceID() + " " + argCollider.name);
        Colliders.Add(argCollider.transform.GetInstanceID(), argCollider.transform);
    }

    private void OnTriggerExit(Collider argCollider)
    {
        Colliders.Remove(argCollider.transform.GetInstanceID());
    }

    private void OnColliderEnter(Collision argCollider)
    {
        Debug.Log(argCollider.transform.GetInstanceID() + " " + argCollider.transform.name);
        Colliders.Add(argCollider.transform.GetInstanceID(), argCollider.transform);
    }

    private void OnColliderExit(Collision argCollider)
    {
        Colliders.Remove(argCollider.transform.GetInstanceID());
    }

    #endregion
}