﻿using UnityEngine;

public class WispMovement : MonoBehaviour
{
    public  float          wispSpeed = 5, wispSway = 0.5f;
    public  ParticleSystem smoke1,        smoke2;
    public  Light          wispLight;
    private bool           foundPlayer, once;
    private Transform      player;
    private float          timer,     timer2;
    private Color          wispColor, wispColor2, wispColor3;

    // Start is called before the first frame update
    private void Start()
    {
        wispColor  = Color.HSVToRGB(Random.Range(0, 0.999f), 1, 1);
        wispColor2 = Color.HSVToRGB(Random.Range(0, 0.999f), 1, 1);
        wispColor3 = Color.HSVToRGB(Random.Range(0, 0.999f), 1, 1);

        gameObject.GetComponentInChildren<MeshRenderer>().material.color = wispColor;
        var gradient = new Gradient();
        var col      = smoke1.colorOverLifetime;
        gradient.SetKeys(
            new[]
            {
                new(new Color(1, 1, 1), 0.0f), new GradientColorKey(wispColor3, 0.262f),
                new GradientColorKey(wispColor2, 0.497f), new GradientColorKey(wispColor, 1.0f)
            },
            new[]
            {
                new GradientAlphaKey(0.0f,  0.0f), new GradientAlphaKey(0.59f,  0.129f),
                new GradientAlphaKey(0.59f, 0.832f), new GradientAlphaKey(0.0f, 1.0f)
            });
        col.color = gradient;

        col = smoke2.colorOverLifetime;
        gradient.SetKeys(
            new[]
            {
                new(new Color(1, 1, 1), 0.0f), new GradientColorKey(wispColor3, 0.262f),
                new GradientColorKey(wispColor2, 0.497f), new GradientColorKey(wispColor, 1.0f)
            },
            new[]
            {
                new GradientAlphaKey(0.0f,  0.0f), new GradientAlphaKey(0.59f,  0.129f),
                new GradientAlphaKey(0.59f, 0.832f), new GradientAlphaKey(0.0f, 1.0f)
            });
        col.color = gradient;

        wispLight.color = wispColor;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!foundPlayer)
        {
            transform.Translate(transform.forward * (wispSpeed        * Time.deltaTime));
            transform.Translate(transform.right   * (Mathf.Sin(timer) * wispSway));
        }
        else
        {
            if (timer2 > 1f)
            {
                if (!once)
                {
                    player.parent.GetComponent<CarController>().randomizeColors = true;
                    gameObject.GetComponentInChildren<MeshRenderer>().enabled   = false;
                    Destroy(gameObject, 20f);
                    once = true;
                }
            }
            else
            {
                timer2             += Time.deltaTime * 0.5f;
                transform.position =  Vector3.Slerp(transform.position, player.position, timer2);
            }
        }

        timer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            player      = other.transform.parent;
            foundPlayer = true;
        }
    }
}