﻿using UnityEngine;

public class SkyboxAninmator : MonoBehaviour
{
    public Material citySkybox;
    public float    speedMultiplier = 0.1f;

    private float timer;

    // Start is called before the first frame update
    private void Start() { }

    // Update is called once per frame
    private void Update()
    {
        citySkybox.SetTextureOffset("_StarsTexture", new Vector2(timer, timer));
        timer += Time.deltaTime * speedMultiplier;
    }
}