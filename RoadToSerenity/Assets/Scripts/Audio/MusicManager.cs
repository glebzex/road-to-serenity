﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{
    public AudioClip[] songs;

    public Text songName;


    [HideInInspector]
    public AudioSource _source;

    //private AudioClip _previousSong;
    //private AudioClip _currentSong;
    private AudioClip _as;

    private void Awake()
    {
        _source      = GetComponent<AudioSource>();
        _source.clip = songs[Random.Range(0, songs.Length)];
    }

    private void Start()
    {
        PlayRandomSong();
        songName.gameObject.SetActive(false);
    }

    private void Update()
    {
        _as = gameObject.GetComponent<AudioSource>().clip;

        if (!_source.isPlaying)
        {
            PlayRandomSong();
        }

        //todo    Remove this before the final build, it changes song for testing purposes.
        /*
        if (Input.GetKeyDown(KeyCode.Y))
        {
            PlayRandomSong();
        }
        */
    }

    private void PlayRandomSong()
    {
        _source.clip = _source.clip == null
            ? songs.RandomElement()
            : songs.Where(t => t != _source.clip).ToList().RandomElement();
        _source.Play();

        _as = gameObject.GetComponent<AudioSource>().clip;

        StartCoroutine(nameof(showSong));
    }


    //public Text songName;

    private IEnumerator showSong()
    {
        Debug.Log(_as);

        songName.text = _as.name;
        songName.gameObject.SetActive(true);

        yield return new WaitForSeconds(15f);

        songName.gameObject.SetActive(false);
    }
}