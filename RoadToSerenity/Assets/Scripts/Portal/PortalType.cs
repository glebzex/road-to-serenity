﻿public enum PortalType
{
    TunnelEntrance,
    TunnelExit,
    PyramidEntrance,
    SpawnTunnelOut,
    SpawnPoint
}