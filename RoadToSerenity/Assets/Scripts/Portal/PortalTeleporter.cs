﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using WorldGeneration;

namespace Portal
{
    public class PortalTeleporter : MonoBehaviour
    {
        public PortalType portalType = PortalType.PyramidEntrance;

        private void Start()
        {
            PortalManager.instance.PortalList.Add(transform);
        }

        private void OnDisable()
        {
            PortalManager.instance.PortalList.Remove(transform);
        }

        private void OnDestroy()
        {
            PortalManager.instance.PortalList.Remove(transform);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && PortalManager.instance.canEnterPortal)
            {
                Teleport();
                StartCoroutine(WaitForPortalDelay());
            }
        }

        private void Teleport()
        {
            if (!PortalManager.instance.canEnterPortal)
            {
                return;
            }

            Transform chosenTransform = null;
            if (portalType == PortalType.PyramidEntrance)
            {
                PortalManager.instance.enteredPortalTransform = transform;
                chosenTransform                               = PortalManager.instance.tunnelIn;
            }
            else if (portalType == PortalType.TunnelExit)
            {
                chosenTransform = PortalManager.instance.RandomPortal;

                if (Random.value < PortalManager.instance.chanceToChangeBiome / 100f || chosenTransform == null)
                {
                    PortalManager.changedBiome = true;
                    GeneratorManager.LoadDifferentRealmNextStart();
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }
            }
            else if (portalType == PortalType.TunnelEntrance)
            {
                chosenTransform = PortalManager.instance.enteredPortalTransform;
            }
            else if (portalType == PortalType.SpawnTunnelOut)
            {
                chosenTransform = PortalManager.instance.RandomPortal;

                if (chosenTransform == null)
                {
                    if (chosenTransform == null)
                    {
                        chosenTransform = PortalManager.instance.PortalList.Find(t =>
                                                                                     t.GetComponent<PortalTeleporter>()
                                                                                      .portalType ==
                                                                                     PortalType.SpawnPoint);
                    }
                }

                WorldGenerator.PreloadMapAtPoint(chosenTransform.position);
            }

            foreach (Transform child in chosenTransform)
            {
                if (child.CompareTag("SpawnPoint"))
                {
                    chosenTransform = child;
                }
            }

            if (chosenTransform == null)
            {
                Debug.Log("No fucking portal found! HOW?!?!?!?!?!?");
                chosenTransform = PortalManager.instance.PortalList.First(t => t != null);
            }

            PortalManager.Teleport(chosenTransform);
        }

        private static IEnumerator WaitForPortalDelay()
        {
            PortalManager.instance.canEnterPortal = false;
            yield return new WaitForSeconds(PortalManager.instance.portalDelay);
            PortalManager.instance.canEnterPortal = true;
        }
    }
}