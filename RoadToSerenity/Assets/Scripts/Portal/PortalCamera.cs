﻿using UnityEngine;

public class PortalCamera : MonoBehaviour
{
    public Transform playerCamera;
    public Transform portal;
    public Transform otherPortal;

    // Start is called before the first frame update
    private void Start()
    {
        playerCamera = GameObject.FindWithTag("Player").GetComponentInChildren<Camera>().transform;
    }

    // Update is called once per frame
    private void Update()
    {
        var playerOffSetFromPortal = playerCamera.position - otherPortal.position;
        transform.position = portal.position + playerOffSetFromPortal;

        var angularDifferenceBetweenPortalRotations = Quaternion.Angle(portal.rotation, otherPortal.rotation);

        var portalRotationalDifference = Quaternion.AngleAxis(angularDifferenceBetweenPortalRotations, Vector3.up);
        var newCameraDirection         = portalRotationalDifference * playerCamera.forward;
        transform.rotation = Quaternion.LookRotation(newCameraDirection, Vector3.up);
    }
}