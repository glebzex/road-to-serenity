﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Spawn;
using UnityEngine;

namespace Portal
{
    [DisallowMultipleComponent]
    public class PortalManager : MonoBehaviour
    {
        public static PortalManager instance;

        public static Transform player;

        public static bool changedBiome;

        [Range(1, 100)]
        public int chanceToChangeBiome = 40;

        public float portalDelay    = 1f;
        public bool  canEnterPortal = true;

        public List<Transform> PortalList = new();

        public Transform enteredPortalTransform;

        public Transform tunnelIn
        {
            get
            {
                return PortalList.FirstOrDefault(t =>
                                                     t.GetComponent<PortalTeleporter>().portalType ==
                                                     PortalType.TunnelEntrance);
            }
        }

        public Transform tunnelOut
        {
            get
            {
                return PortalList.FirstOrDefault(t =>
                                                     t.GetComponent<PortalTeleporter>().portalType ==
                                                     PortalType.TunnelExit);
            }
        }

        public Transform RandomPortal
        {
            get
            {
                var availablePortals = PortalList.Where(t =>
                                                            t != enteredPortalTransform &&
                                                            t.GetComponent<PortalTeleporter>().portalType ==
                                                            PortalType.PyramidEntrance);

                if (!availablePortals.Any())
                {
                    Debug.LogWarning($"{"Portal: ".Color(Color.red)} No exit portal!");
                    return null;
                }

                return availablePortals.RandomElement();
            }
        }

        private void Awake()
        {
            instance = this;

            if (PortalList.Count > 0)
            {
                PortalList.Clear();
            }

            if (changedBiome)
            {
                Debug.Log("Changed biome".Color(Color.green));
                Teleport(RandomPortal, true);
                changedBiome = false;
            }

            StartCoroutine(nameof(LoadSpecialPortals));
        }

        public static void Teleport(Transform toTransform, bool preloadAtExit = false)
        {
//            if (preloadAtExit)
//                WorldGenerator.PreloadMapAtPoint(toTransform.position);

            player.position = toTransform.position;
            player.rotation = toTransform.rotation;
            player.GetComponent<Rigidbody>().velocity =
                player.forward *
                player.GetComponent<Rigidbody>().velocity.magnitude;
        }

        private IEnumerator LoadSpecialPortals()
        {
            yield return new WaitForEndOfFrame();

            player = FindObjectOfType<RaycastSpawn>().transform;
        }
    }
}