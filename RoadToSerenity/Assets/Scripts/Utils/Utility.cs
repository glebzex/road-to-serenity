﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public static class Utility
{
//    /// <summary>
//    /// Returns a dynamic list of child objects.
//    /// If the object has no children, returns null.
//    /// </summary>
//    /// <param name="parent"></param>
//    /// <returns></returns>
    public static List<GameObject> GetChildrenInList(this GameObject parent)
    {
        var list = new List<GameObject>();

        if (parent.transform.childCount == 0)
        {
            return null;
        }

        for (var i = 0; i < parent.transform.childCount; i++)
        {
            list.Add(parent.transform.GetChild(i).gameObject);
        }

        return list;
    }

    public static string Color(this string stringToColor, Color color)
    {
        return $"<color=#{ColorUtility.ToHtmlStringRGBA(color)}>{stringToColor}</color>";
    }

    public static List<T> GetChildrenInList<T>(this GameObject parent)
    {
        var list = new List<T>();

        if (parent.transform.childCount == 0)
        {
            return null;
        }

        for (var i = 0; i < parent.transform.childCount; i++)
        {
            list.Add(parent.transform.GetChild(i).GetComponent<T>());
        }

        return list;
    }

    public static List<T> Collect<T>(this IEnumerable enumerable)
    {
        var list = new List<T>();
        foreach (T item in enumerable)
        {
            list.Add(item);
        }

        return list;
    }

    public static T[] Shuffle<T>(this T[] array)
    {
        var n = array.Length;

        for (var i = 0; i < n; i++)
        {
            var r = i + Random.Range(0, n - i);
            var t = array[r];
            array[r] = array[i];
            array[i] = t;
        }

        return array;
    }

    public static List<T> Shuffle<T>(this List<T> list)
    {
        var n = list.Count;

        for (var i = 0; i < n; i++)
        {
            var r = i + Random.Range(0, n - i);
            var t = list[r];
            list[r] = list[i];
            list[i] = t;
        }

        return list;
    }

    public static T RandomElement<T>(this T[] array)
    {
        if (array.Length == 1)
        {
            return array[0];
        }

        if (array.Length == 0)
        {
            throw new Exception("Array is empty!");
        }

        return array[Random.Range(0, array.Length)];
    }

    public static T RandomElement<T>(this List<T> list)
    {
        if (list.Count == 1)
        {
            return list[0];
        }

        if (list.Count == 0)
        {
            Debug.LogWarning("List is empty!");
        }

        return list[Random.Range(0, list.Count)];
    }

    public static T RandomElement<T>(this IEnumerable<T> enumerable)
    {
        var count = enumerable.Count();
        if (count == 1)
        {
            return enumerable.ElementAt(0);
        }

        if (count == 0)
        {
            Debug.LogWarning("IEnumerable is empty!");
        }

        return enumerable.ElementAt(Random.Range(0, count));
    }

    public static void ForEach<T>(this IEnumerable<T> objects, Action<T> action)
    {
        objects.ToList().ForEach(action);
    }

    public static T Log<T>(this T objectToLog)
    {
        Debug.Log(objectToLog);
        return objectToLog;
    }
}