﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class DrawNormals : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField]
    private MeshFilter _meshFilter;

    [SerializeField]
    private NormalsDrawData _faceNormals = new(new Color32(34, 221, 221, 155), true);

    [SerializeField]
    private NormalsDrawData _vertexNormals = new(new Color32(200, 255, 195, 127), false);

    [Serializable]
    private class NormalsDrawData
    {
        private const float _baseSize = 0.0125f;

        [SerializeField]
        protected DrawType _draw = DrawType.Selected;

        [SerializeField]
        protected float _length = 0.3f;

        [SerializeField]
        protected Color _normalColor;

        private Color _baseColor = new Color32(255, 133, 0, 255);


        public NormalsDrawData(Color normalColor, bool draw)
        {
            _normalColor = normalColor;
            _draw        = draw ? DrawType.Selected : DrawType.Never;
        }

        public bool CanDraw(bool isSelected)
        {
            return _draw == DrawType.Always || (_draw == DrawType.Selected && isSelected);
        }

        public void Draw(Vector3 from, Vector3 direction)
        {
            if (Camera.current.transform.InverseTransformDirection(direction).z < 0f)
            {
                Gizmos.color = _baseColor;
                Gizmos.DrawWireSphere(from, _baseSize);

                Gizmos.color = _normalColor;
                Gizmos.DrawRay(from, direction * _length);
            }
        }

        protected enum DrawType
        {
            Never,
            Selected,
            Always
        }
    }

    private void OnDrawGizmosSelected()
    {
        EditorUtility.SetSelectedRenderState(GetComponent<Renderer>(), EditorSelectedRenderState.Hidden);
        OnDrawNormals(true);
    }

    private void OnDrawGizmos()
    {
        if (!Selection.Contains(this))
        {
            OnDrawNormals(false);
        }


        if (_meshFilter.sharedMesh != null)
        {
            foreach (var vert in _meshFilter.sharedMesh.vertices)
            {
                Gizmos.DrawWireSphere(transform.TransformPoint(vert), 0.001f);
            }
        }
    }

    private void OnDrawNormals(bool isSelected)
    {
        if (_meshFilter == null)
        {
            _meshFilter = GetComponent<MeshFilter>();
            if (_meshFilter == null)
            {
                return;
            }
        }

        var mesh = _meshFilter.sharedMesh;

        //Draw Face Normals
        if (_faceNormals.CanDraw(isSelected) && mesh != null)
        {
            var triangles = mesh.triangles;
            var vertices  = mesh.vertices;

            for (var i = 0; i < triangles.Length; i += 3)
            {
                var v0     = transform.TransformPoint(vertices[triangles[i]]);
                var v1     = transform.TransformPoint(vertices[triangles[i + 1]]);
                var v2     = transform.TransformPoint(vertices[triangles[i + 2]]);
                var center = (v0 + v1 + v2) / 3;

                var dir = Vector3.Cross(v1 - v0, v2 - v0);
                dir /= dir.magnitude;

                _faceNormals.Draw(center, dir);
            }
        }

        //Draw Vertex Normals
        if (_vertexNormals.CanDraw(isSelected) && mesh != null)
        {
            var vertices = mesh.vertices;
            var normals  = mesh.normals;
            for (var i = 0; i < vertices.Length; i++)
            {
                _vertexNormals.Draw(transform.TransformPoint(vertices[i]), transform.TransformVector(normals[i]));
            }
        }
    }
#endif
}