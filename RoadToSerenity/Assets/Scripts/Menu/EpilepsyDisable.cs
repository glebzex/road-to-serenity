﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EpilepsyDisable : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        if (Input.anyKey)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1, LoadSceneMode.Single);
        }
    }
}