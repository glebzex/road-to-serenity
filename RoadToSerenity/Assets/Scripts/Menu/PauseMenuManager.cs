﻿using Rewired;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject pauseMenu, settingsMenu, settingsBackButton, continueButton_btn, controlsBackButton, controlsMenu;
    public Slider mainSlider, sfxSlider, musicSlider;
    public GameObject gameManager;
    private Player _playerController;
    private GameObject car;

    private bool    isActive;
    private Vector3 selectedObjectTransform;


    private void Start()
    {
        pauseMenu.SetActive(false);
        isActive          = false;
        Time.timeScale    = 1f;
        mainSlider.value  = PlayerPrefs.GetFloat("mainVolume",  0.5f);
        sfxSlider.value   = PlayerPrefs.GetFloat("sfxVolume",   0.5f);
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume", 0.5f);
        Cursor.visible    = false;
        car               = GameObject.Find("CarWithNewCamera(Clone)");
    }

    // Update is called once per frame
    private void Update()
    {
        _playerController = ReInput.players.GetPlayer(0);
        if (_playerController.GetButtonDown("Menu"))
        {
            if (!isActive)
            {
                pauseMenu.SetActive(true);
                Cursor.visible = true;
                isActive       = true;
                EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(continueButton_btn);
                Time.timeScale = 0f;
            }
            else
            {
                pauseMenu.SetActive(false);
                settingsMenu.SetActive(false);
                controlsMenu.SetActive(false);
                isActive       = false;
                Time.timeScale = 1f;
            }
        }

        if (!car)
        {
            car = GameObject.Find("CarWithNewCamera(Clone)"); // change this when we change car
        }

        if (!gameManager)
        {
            gameManager = GameObject.Find("GameManager");
        }

        if (EventSystem.current.currentSelectedGameObject == null && pauseMenu.activeSelf)
        {
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(continueButton_btn);
        }

        if (EventSystem.current.currentSelectedGameObject == null && settingsMenu.activeSelf)
        {
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(settingsBackButton);
        }

        if (EventSystem.current.currentSelectedGameObject == null && controlsMenu.activeSelf)
        {
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(controlsBackButton);
        }
    }

    public void continueButton()
    {
        pauseMenu.SetActive(false);
        isActive       = false;
        Time.timeScale = 1f;
    }

    public void menuButton()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        Time.timeScale = 1f;
    }

    public void MainVolumeSlider(float mainVolume)
    {
        var sliderValue = AudioListener.volume;
        sliderValue          = mainVolume;
        AudioListener.volume = sliderValue;
        PlayerPrefs.SetFloat("mainVolume", sliderValue);
        PlayerPrefs.Save();
    }

    public void sfxVolumeSlider(float sfxVolume)
    {
        car.GetComponentInChildren<SoundController>().engineSource1.volume      = sfxVolume;
        car.GetComponentInChildren<SoundController>().skidSource.volume         = sfxVolume;
        car.GetComponentInChildren<SoundController>().shiftUpSource.volume      = sfxVolume;
        car.GetComponentInChildren<SoundController>().shiftDownSource.volume    = sfxVolume;
        car.GetComponentInChildren<SoundController>().blowOffValveSource.volume = sfxVolume;

        PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
        PlayerPrefs.Save();
    }

    public void musicVolumeSlider(float musicVolume)
    {
        gameManager.GetComponentInChildren<MusicManager>()._source.volume = musicVolume;
        PlayerPrefs.SetFloat("musicVolume", musicVolume);
        PlayerPrefs.Save();
    }


    public void openSettingMenu()
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(settingsBackButton);
    }

    public void openMainMenu()
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(continueButton_btn);
    }

    public void openControlsMenu()
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(controlsBackButton);
    }
}