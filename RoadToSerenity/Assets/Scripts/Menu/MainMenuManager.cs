﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    public GameObject controlsBackButton, mainPlayButton, mainPanel, controlsPanel;

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null && mainPanel.activeSelf)
        {
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(mainPlayButton);
        }

        if (EventSystem.current.currentSelectedGameObject == null && controlsPanel.activeSelf)
        {
            EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(controlsBackButton);
        }
    }


    public void exitButton()
    {
        Application.Quit();
    }

    public void openControlsMenu()
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(controlsBackButton);
    }

    public void openMainMenu()
    {
        EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(mainPlayButton);
    }
}